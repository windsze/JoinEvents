package com.winds.joinevents;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class loginActivity extends AppCompatActivity {

    // UI
    private TextInputLayout mUsername_input, mPassword_input;
    private TextView mUsername_helper, mPassword_helper;
    private Button mCreateAccount, mLogin;
    private LoginButton mFacebook_login_original;


    private CallbackManager callbackManager;
    // VARIABLE
    private JSONHandler mJsonHandler;
    private NetworkHandler mNetworkHandler;
    private Handler mHandler;
    private String username, password;
    private UserHandler mUserHandler;
    private EventHandler mEventHandler;
    private User user;

    // FLAGS
    private final int DUPLICATED_NAME = 1;
    private final int CREATE_ACCOUNT_SUCCESS = 3;
    private final int CREATE_ACCOUNT_FAIL = 4;
    private final int THREAD_RUN_DONE = 5;
    private final int REGISTER_SUCCESS = 6;
    private final int REGISTER_FAIL = 7;
    private final int NO_REGISTER = 8;
    private final int YES_REGISTERED = 9;

    // TAG
    private final String TAG = "loginActivity";






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this);
        callbackManager = CallbackManager.Factory.create();

        setContentView(R.layout.activity_login);

        // INIT UI
        initUIComponents();

        // INIT CLASS
        mJsonHandler = new JSONHandler();
        mEventHandler = new EventHandler(getApplicationContext());
        mUserHandler = new UserHandler(getApplicationContext());

        mNetworkHandler = new NetworkHandler(this);
        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case DUPLICATED_NAME:
                        Toast.makeText(loginActivity.this, "Duplicated Username!", Toast.LENGTH_SHORT).show();
                        break;
                    case THREAD_RUN_DONE:
                        mLogin.setEnabled(true);
                        break;
                    case REGISTER_SUCCESS:
                        // register success
                        // login will also be success
                        Toast.makeText(getApplicationContext(),"Register success!",Toast.LENGTH_SHORT).show();

                        // todo. store all info into sharedPreferenceManager


                        startActivity(new Intent(loginActivity.this, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                        break;
                    case REGISTER_FAIL:
                        Toast.makeText(getApplicationContext(),"Register fail!",Toast.LENGTH_SHORT).show();
                        finish();
                        break;
                    case NO_REGISTER:
                        Toast.makeText(getApplicationContext(),"We have no your email record",Toast.LENGTH_LONG).show();
                        mCreateAccount.performClick();
                        break;
                    case YES_REGISTERED:
                        Toast.makeText(getApplicationContext(),"Please wait...",Toast.LENGTH_LONG).show();
                        break;

                }
                super.handleMessage(msg);
            }
        };

        setupOnclickListener();
    }


    private void initUIComponents() {
        mUsername_input = (TextInputLayout) findViewById(R.id.username_textInput);
        mUsername_helper = (TextView) findViewById(R.id.username_helper);

        mPassword_input = (TextInputLayout) findViewById(R.id.password_textInput);
        mPassword_helper = (TextView) findViewById(R.id.password_helper);

        mCreateAccount = (Button) findViewById(R.id.btnRegister_login);
        mLogin = (Button) findViewById(R.id.btnSubmit_login);

        mFacebook_login_original = (LoginButton) findViewById(R.id.login_button);

    }

    private void setupOnclickListener() {

        mCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo. show dialog ask user signup methods
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
//

                        MaterialDialog signupMaterialDialog = new MaterialDialog.Builder(loginActivity.this)
                                .title("Sign up via : ")
                                .customView(R.layout.signup_dialog_layout, true)
                                .show();
//

                        View view = signupMaterialDialog.getCustomView();
                        view.findViewById(R.id.imageView_facebook_signup).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //todo perform facebook button onclick
                                //
                                com.facebook.login.widget.LoginButton btn = new LoginButton(loginActivity.this);
                                List<String> permissions = new ArrayList<String>();
                                permissions.add("public_profile");
                                permissions.add("email");
                                permissions.add("user_birthday");

                                btn.setReadPermissions(permissions);
                                btn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        Log.v(TAG, "facebook LoginButton onclick");
                                    }
                                });
                                btn.performClick();
                            }
                        });

                        view.findViewById(R.id.imageView_joinevent_signup).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                startActivity(new Intent(loginActivity.this, registerActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            }
                        });

                        // start time consuming background process here
                    }
                }, 1); // starting it in 1 second




                //startActivity(new Intent(loginActivity.this, registerActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            }
        });


        // Callback registration
        mFacebook_login_original.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                Log.v(TAG, "onSuccess login");

                AccessToken accessToken = loginResult.getAccessToken();
                Profile profile = Profile.getCurrentProfile();

                if (profile != null) {
                    Log.v(TAG, "profile got!");
                    Log.v(TAG, "username: " + profile.getName());
                }

                GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(final JSONObject object, GraphResponse response) {
                        // Insert your code here

                        Log.v(TAG, object.toString());

                        try {
                            // every facebook account must have a email account
                            if (object.getString("email")!=null){

                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            if (mUserHandler.isUserExist(object.getString("email"))){
                                                // email has registered
                                                SharedPreferenceManager sharedPreferenceManager = new SharedPreferenceManager(getApplicationContext());
                                                sharedPreferenceManager.createLoginSession(); // now user is logged in

                                                Log.v(TAG,"create login session done");
                                                // get user detail in a User object from sharedPreference to display in mainActivity
                                                user = mUserHandler.getAllUserDetails(object.getString("email"));

                                                Log.v(TAG, "get user detail done");

                                                // save user details in sharedPreference
                                                sharedPreferenceManager.storeUserDetails(user);

                                                Log.v(TAG, "save user details in sharedPreference done");

                                                startActivity(new Intent(loginActivity.this, MainActivity.class));

                                            }else {
                                                // email has not registered
                                                Log.v(TAG,"Email has not registered");
                                                //todo create new user, use fb_id as password, prompt user to enter new password
                                                /*
                                                * parameters we need:
                                                * 1. email
                                                * 2. Xusername :: facebook doesnt support anymore
                                                * 3. password
                                                * 4. birthday :: maybe empty
                                                * 5. full name
                                                * 6. gender
                                                *
                                                * so we need to hv another layout for getting password and username
                                                * pass the JSON Object to the new activity
                                                * */
                                                // start activity and get username and password
                                                user = new User();
                                                user.setName(object.getString("name"));
                                                user.setGender(object.getString("gender"));
                                                user.setEmail(object.getString("email"));
//                                                if (!object.getString("birthday").isEmpty()) {
//                                                    String pattern = "dd-MMM-yyyy";
//                                                    user.dob = DateTime.parse(object.getString("birthday"), DateTimeFormat.forPattern(pattern));
//                                                }

                                                Intent intent = new Intent(loginActivity.this, getMoreUserInfoActivity.class);
                                                Bundle bundle = new Bundle();
                                                bundle.putSerializable("user",user);
                                                intent.putExtras(bundle);
                                                startActivityForResult(intent, 1); // request code for identifying where the request from


                                            }
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }).start();

                            }else{
                                Log.v(TAG,"No Email Account Found");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,birthday,email, name,gender");
                request.setParameters(parameters);
                request.executeAsync();
            }


            @Override
            public void onCancel() {
                // App code
                Log.v(TAG, "onCancel");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.v(TAG, "OnError");
            }
        });

        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v(TAG, "Join Event Login Button On Click");

                final String email = ((EditText)findViewById(R.id.login_name)).getText().toString();
                String password = ((EditText)findViewById(R.id.login_password)).getText().toString();

                // check is registered
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (mUserHandler.isUserExist(email)){
                                // has registered
                                mHandler.sendEmptyMessage(YES_REGISTERED);
                                // check password
                            }else{
                                // not yet registered
                                // bring him into dialog signup page
                                mHandler.sendEmptyMessage(NO_REGISTER);

                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();

                // if enter all correct
//                mUserHandler.changeLoginType(1);
            }
        });



    }


    private String getObjectViaName(JSONObject object, String name) throws JSONException {
        if (object.has(name))
            return object.getString(name);

        return null;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        callbackManager.onActivityResult(requestCode, resultCode, data);

        /*
        * get result from sub-activity (getMoreUserInfoActivity)
        * 1. username
        * 2. password
        * */
        if (requestCode == 1){
            // from login activity
            if (resultCode == RESULT_OK){
                Log.v(TAG, "Result ok");

                Bundle bundle = data.getExtras();
                user = (User) bundle.getSerializable("user");

                username = user.getUsername();
                password = user.getPassword();

                Log.v(TAG, "username and password got back are: " + username + "," + password);

                DateTime nowTime = new DateTime();
                user.setRegistrationDate(nowTime.toString());
                user.setUserType(0);
                user.setLoginType(0); // facebook
                user.setPoint(0);
                user.setLast_login_date(nowTime.toString());


                setContentView(R.layout.progress_layout);
                // todo put all info to db
                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        if (mUserHandler.createNewUser(user)){
                            mHandler.sendEmptyMessage(REGISTER_SUCCESS);
                        }

                        else
                            mHandler.sendEmptyMessage(REGISTER_FAIL);
                    }
                }).start();




            }else if(resultCode == RESULT_CANCELED){
                Log.v(TAG, "Result canceled");
                Toast.makeText(this, "Something get wrong!", Toast.LENGTH_SHORT);
                LoginManager.getInstance().logOut();

            }else
                Log.v(TAG,"resultCode fail??");

            finish();
        }
    }


    private boolean isUsernameValid() {
        //todo. check username constructions
        if (mUsername_input.getEditText().toString().matches("^[a-z0-9_-]{1,10}$"))
            return true;

        return false;
    }

    private boolean isPasswordValid() {
        // check password
        if (mPassword_input.getEditText().toString().matches("^[a-z0-9_-]{6,22}$"))
            return true;

        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();


    }
}
