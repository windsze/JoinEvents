package com.winds.joinevents;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

/**
 * This class handles all notification-related activities
 */
public class NotificationHandler {
    private SQLiteHelper sqLiteHelper;
    private Context _context;
    private final String TAG = "NotificationHandler";
    private LocationQueryHandler locationServerHandler;

    // Service
    private NotificationManager mNotificationManager;
    private Notification mNotification;
    private NotificationCompat.Builder mBuilder;
    private final int NOTIFY_ID = 1;
    private int count = 1;
    final boolean autoCancel = true; // 點擊通知後是否要自動移除掉通知


    /**
     * ==============================
     * Method
     * ==============================
     */

    public NotificationHandler(Context context) {
        _context = context;

        init();
    }


    public void addEvent(Event targetEvent) {
        // get back db
        SQLiteDatabase db = sqLiteHelper.getWritableDatabase();

        // store values
        ContentValues values = new ContentValues();
        values.put("_id", targetEvent.getEventId());
        values.put("_name", targetEvent.getName());
        values.put("_lat", targetEvent.getLat());
        values.put("_lng", targetEvent.getLng());

//        // store values
//        String sql = "insert into "+sqLiteHelper.getTableName()+" (_id,_name,_lat,_lng) values ("
//                + targetEvent.getEventId() + ","
//                + targetEvent.getName() + ","
//                + targetEvent.getLat() + ","
//                + targetEvent.getLng() + ");";
//
//        Log.e(TAG, "the sql statement in AddEvent is:"+sql);
//
//        // insert record
//        db.execSQL(sql);

        // insert record
        db.insert(sqLiteHelper.getTableName(), null, values);

        // release db memory
        closeDB();

    }

    public void deleteEvent(Event targetEvent) {
        // get back db
        SQLiteDatabase db = sqLiteHelper.getWritableDatabase();

        // sql to delete record
        db.execSQL("DELETE FROM " + sqLiteHelper.getTableName() + " WHERE _id =" + targetEvent.getEventId());

        // release db memory
        closeDB();
    }

    /**
     * This checks all events which are within @param in db and return close events list to user.
     *
     * @param currentGeocode User's current location
     * @return All close events
     */
    public List<Event> searchCloseEvent(LatLng currentGeocode, int distance) {
        List<Event> closeEvents = new ArrayList<>();

        // get back db
        SQLiteDatabase db = sqLiteHelper.getReadableDatabase();

        // get back all events from db
        Cursor cursor = db.rawQuery("SELECT * FROM " + sqLiteHelper.getTableName(), null);

        // move to first row by using cursor as pointer
        cursor.moveToFirst();

        // read record one by one
        while (!cursor.isAfterLast()) {
            int event_id = cursor.getInt(0);
            LatLng event_latlng = new LatLng(cursor.getFloat(2), cursor.getFloat(3));
            String event_name = cursor.getString(1);

            // parse into event object item
            Event event = new Event();
            event.setName(event_name);
            event.setEventId(event_id);
            event.setLat(String.valueOf(event_latlng.latitude));
            event.setLng(String.valueOf(event_latlng.longitude));

            Log.e(TAG, "_id = " + event_id);
            Log.e(TAG, "_name = " + event_name);
            Log.e(TAG, "_lat = " + event_latlng.latitude);
            Log.e(TAG, "_lng = " + event_latlng.longitude);

            // check latlng
            int distanceBetween = (int) locationServerHandler.getDistance(currentGeocode, event_latlng);
            Log.w(TAG, "distanceBetween: " + distanceBetween);
            if (distanceBetween < distance) {
                Log.e(TAG,"close event found! add to list...");
                // location within 500 meters
                Log.w(TAG, "location are within 1500 meters");
                closeEvents.add(event);
            }else{
                Log.w(TAG, "location are without 1500 meters ?? is it?");

            }

            cursor.moveToNext();

        }


        cursor.close();
        closeDB();

        return closeEvents;
    }

    public List<Integer> getEventIdList(){
        List<Integer> result = sqLiteHelper.getEventIdList();

        return result;
    }

    private void init() {
        sqLiteHelper = new SQLiteHelper(_context);
        locationServerHandler = new LocationQueryHandler(_context);

    }

    public void closeDB() {
        sqLiteHelper.close();
    }


}
