package com.winds.joinevents;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;

import java.util.ArrayList;

/**
 * Created by WindS on 9/11/15.
 */
public class DetectionIntentService extends IntentService{
    /* This class by default will run in separate thread through the initialize from the intent from main thread*/
    private static final String TAG = "DetectionIntentHandler";
    private static final String PACKAGE_NAME = "com.winds.joinevents";
    private static final String BROADCAST_ACTION = PACKAGE_NAME + ".BROADCAST_ACTION";

    private Context context;

    public DetectionIntentService(Context comingContext) {
        super(TAG); // tag the name of the class
        context = comingContext;
    }

    /**
     * Returns a human readable String corresponding to a detected activity type.
     */
    private static String getActivityString(Context context, int detectedActivityType) {
        Resources resources = context.getResources();
        switch (detectedActivityType) {
            case DetectedActivity.IN_VEHICLE:
                return "In Vehicle";
            case DetectedActivity.ON_BICYCLE:
                return "On Bicyle";
            case DetectedActivity.ON_FOOT:
                return "On Foot";
            case DetectedActivity.RUNNING:
                return "Running";
            case DetectedActivity.STILL:
                return "Still";
            case DetectedActivity.TILTING:
                return "Tilting";
            case DetectedActivity.UNKNOWN:
                return "Sorry! Unknown";
            case DetectedActivity.WALKING:
                return "Walking";
            default:
                return "Others.." + detectedActivityType;
        }
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        DetectedActivity mostLikelyActivity = null;
        int mostConfiendce = 0;

        ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);
        // Get the list of the probable activities associated with the current state of the
        // device. Each activity is associated with a confidence level, which is an int between
        // 0 and 100.
        ArrayList<DetectedActivity> detectedActivities = (ArrayList) result.getProbableActivities();

        // Log each activity.
        Log.i(TAG, "activities detected");
        for (DetectedActivity da : detectedActivities) {

            // todo check the higest confidence
            // todo return the action type to the user
            if (da.getConfidence() > mostConfiendce){
                mostConfiendce = da.getConfidence();
                mostLikelyActivity = da;
            }

            Log.i(TAG, getActivityString(
                            context,
                            da.getType()) + " " + da.getConfidence() + "%"
            );
        }


        // Broadcast the list of detected activities.

        //todo sample use broadcasr manager to do it, check for it;;
        Intent localIntent = new Intent(BROADCAST_ACTION);
        localIntent.putExtra("MostLikelyActivity", mostLikelyActivity);
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
    }
}
