package com.winds.joinevents;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

public class getMoreUserInfoActivity extends AppCompatActivity {

    //UI
    TextView mEmail_data, mUsername_helper, mPassword_helper;
    TextInputLayout mUsername_input, mPassword_input;
    Button mSubmit, mDismiss;

    // variable
    String emailAddress;

    //tag
    private final String TAG = "getMoreUserInfoActivity";

    // User object
    User user;

    //Handler
    Handler handler;

    // FLAG
    private final int USERNAME_OK = 0;
    private final int USERNAME_DUPLICATED = 1;
//    private final int

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_more_user_info);


        setupUI();
        setupOnClick();

        // should receive intent containing email
        Intent intent = getIntent();
        if (intent!=null){
            user = (User)intent.getExtras().getSerializable("user");
            // set email textview
            mEmail_data.setText(user.getEmail());
        }else{
            Log.v(TAG,"no intent found");
            Toast.makeText(this, "User account goes wrong..",Toast.LENGTH_SHORT).show();
            finish();
        }

        handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);

                switch (msg.what){
                    case USERNAME_DUPLICATED:
                        // username is exist
                        mUsername_helper.setText(mUsername_input.getEditText().getText().toString() + " have been registered already");
                        isNameOK = false;
                        break;
                    case USERNAME_OK:
                        mUsername_helper.setText(mUsername_input.getEditText().getText().toString() + " can be used.");
                        isNameOK = true;
                        user.setUsername(mUsername_input.getEditText().getText().toString());
                        break;
                }
            }

        };

    }
    boolean isNameOK;
    boolean isPassOK;
    ProgressDialog progressDialog;
    private void setupOnClick() {
        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                 progressDialog = ProgressDialog.show(getMoreUserInfoActivity.this, "",
                        "Loading. Please wait...", true);


                //todo check all fill in
                if (mUsername_input.getEditText().length()==0){
                    //no input
                    isNameOK = false;
                    mUsername_helper.setTextColor(getResources().getColor(R.color.bpRed));
                    progressDialog.dismiss();


                }else{
                    mUsername_helper.setTextColor(getResources().getColor(R.color.primary_text));

                    // check existence of username
                    Thread checkUsernameThread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            UserHandler userHandler = new UserHandler(getApplicationContext());
                            try {
                                if (userHandler.isUsernameExist(mUsername_input.getEditText().getText().toString())){
                                    handler.sendEmptyMessage(USERNAME_DUPLICATED);
                                }else{
                                    handler.sendEmptyMessage(USERNAME_OK);
                                }

                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        }
                    });

                    checkUsernameThread.start();

                    // wait for the thread finish
                    try {
                        checkUsernameThread.join();

                        /*
                        * Main loading process has been finished, stop progress dialog
                        * */
                        progressDialog.dismiss();

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }


                }

                if (mPassword_input.getEditText().getText().length()<6 || mPassword_input.getEditText().getText().length()>21){
                    //wrong input
                    isPassOK = false;
                    mPassword_helper.setTextColor(getResources().getColor(R.color.bpRed));
                }else {
                    mPassword_helper.setTextColor(getResources().getColor(R.color.primary_text));
                    isPassOK = true;
                    user.setPassword(mPassword_input.getEditText().getText().toString());
                }


                if (isNameOK && isPassOK){
                    Intent intent = new Intent();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("user",user);
                    intent.putExtras(bundle);
                    setResult(RESULT_OK, intent);
                    finish();
                }
            }
        });

        mDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }

    private void setupUI() {
        mEmail_data = (TextView) findViewById(R.id.mEmail_moreInfo_data);
        mUsername_helper = (TextView) findViewById(R.id.mUsername_helper_moreInfo);
        mPassword_helper = (TextView) findViewById(R.id.mPassword_helper_moreInfo);

        mUsername_input = (TextInputLayout) findViewById(R.id.mUsername_input);
        mPassword_input = (TextInputLayout) findViewById(R.id.mPassword_input);
        mSubmit = (Button) findViewById(R.id.mSubmit_moreInfo);
        mDismiss = (Button) findViewById(R.id.mDismiss);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_get_more_user_info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
