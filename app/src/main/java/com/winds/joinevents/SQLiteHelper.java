package com.winds.joinevents;

import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;


public class SQLiteHelper extends SQLiteOpenHelper {

    private final static int _DBVersion = 1;
    private final static String _DBName = "JoinEvent.db";
    private final static String _TableName = "notification_events";
    private static final String TAG = "SQLiteHelper";

    public SQLiteHelper(Context context) {
//        super(context, name, factory, version);
        super(context, _DBName, null, _DBVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.e(TAG, "onCreate, ready to create the db...");
        /**
         * CREATE TABLE `notification_events` ( `event_id` INT NOT NULL , `event_name` VARCHAR(20) NOT NULL , `event_lat` DOUBLE NOT NULL , `event_lng` DOUBLE NOT NULL ) ENGINE = InnoDB CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
         */
//        final String SQL = "CREATE TABLE IF NOT EXISTS " + _TableName + "( " +
//        "_id INTEGER PRIMARY KEY NOT NULL, " +
//        "_name VARCHAR(30) NOT NULL, " +
//        "_lat DOUBLE NOT NULL," +
//        "_lng DOUBLE NOT NULL" +
//        ") CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ;";
        String SQL = "CREATE TABLE " + _TableName + "( `_id` INT NOT NULL , " +
                "`_name` VARCHAR(20) NOT NULL ," +
                " `_lat` DOUBLE NOT NULL , " +
                "`_lng` DOUBLE NOT NULL ," +
                " PRIMARY KEY (`_id`));";

        db.execSQL(SQL);

        Log.e(TAG, "onCreate, db created");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.e(TAG, "onUpdate...");

        final String SQL = "DROP TABLE " + _TableName;
        db.execSQL(SQL);
        onCreate(db);
    }

    public String getDBName() {
        return _DBName;
    }

    public String getTableName() {
        return _TableName;
    }


    /**
     * Method returns list  of event's id in db
     * @return
     */
    public List<Integer> getEventIdList() {
        List<Integer> idList = new ArrayList<>();

        //get readable db
        SQLiteDatabase db = this.getReadableDatabase();

        // result
        String sql = "select _id from " + getTableName();
        Cursor cursor = db.rawQuery(sql, null);
        cursor.moveToFirst();

        if (cursor.getCount() > 0)
            for (int i = 0; i < cursor.getCount(); i++) {
                idList.add(cursor.getInt(0));
                cursor.moveToNext();
            }

        // release memory
        cursor.close();
        db.close();

        return idList;
    }


    /**
     * This getData method is written by AndroidDataBaseManager developer
     *
     * @param Query
     * @return
     */
    public ArrayList<Cursor> getData(String Query) {
        //get writable database
        SQLiteDatabase sqlDB = this.getWritableDatabase();
        String[] columns = new String[]{"mesage"};
        //an array list of cursor to save two cursors one has results from the query
        //other cursor stores error message if any errors are triggered
        ArrayList<Cursor> alc = new ArrayList<Cursor>(2);
        MatrixCursor Cursor2 = new MatrixCursor(columns);
        alc.add(null);
        alc.add(null);


        try {
            String maxQuery = Query;
            //execute the query results will be save in Cursor c
            Cursor c = sqlDB.rawQuery(maxQuery, null);


            //add value to cursor2
            Cursor2.addRow(new Object[]{"Success"});

            alc.set(1, Cursor2);
            if (null != c && c.getCount() > 0) {


                alc.set(0, c);
                c.moveToFirst();

                return alc;
            }
            return alc;
        } catch (SQLException sqlEx) {
            Log.d("printing exception", sqlEx.getMessage());
            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[]{"" + sqlEx.getMessage()});
            alc.set(1, Cursor2);
            return alc;
        } catch (Exception ex) {

            Log.d("printing exception", ex.getMessage());

            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[]{"" + ex.getMessage()});
            alc.set(1, Cursor2);
            return alc;
        }


    }
}

