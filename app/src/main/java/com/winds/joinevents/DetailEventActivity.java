package com.winds.joinevents;

import android.app.Dialog;
import android.content.Intent;
import android.location.Location;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import mehdi.sakout.dynamicbox.DynamicBox;

public class DetailEventActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private static final String TAG = "DetailEventActivity";

    //FLAG
    private static final int GET_RECOMMENDATION_FINISH = 0;
    private static final int DETAIL_RECOMMEND_VIEW = 2;
    private static final int GET_DETAILS_FINISH = 3;
    private static final int GET_DETAILED_EVENT_FINISH = 4;
    private static final int LIKE_FINISH = 5;
    private static final int DISLIKE_FINISH = 6;

    // UI
    Toolbar mToolbar;
    Event mainEvent;
    CollapsingToolbarLayout mCollapsing_toolbar;
    SliderLayout mImageSlider;
    AppBarLayout mAppBarLayout;
    RecyclerView mDetail_recommend_recycler_view;
    LinearLayoutManager mLayoutManager;

    TextView mLocation, mTime, mDescription, mAuthor;
    Button mNumberOfEventsBtn, mLike, mDislike, mComment, mShare;
    ImageView mAuthorProfile;

    //
    ArrayList<Event> myDatasetArr;
    int mPage = 1;
    int mPage_recommend = 1;
    int totalUserEventNumber;
    // class
    EventHandler eventHandler;
    MyRecyclerAdapter mAdapter;
    UserHandler userHandler;

    //dynamic box
//    DynamicBox box;

    // image loader
    DisplayImageOptions options;

    SharedPreferenceManager preferenceManager;

    // handler
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case GET_DETAILED_EVENT_FINISH:


                    try {
                        setupGoogleMap();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    continueProcess();


                    break;

                case GET_DETAILS_FINISH:

                    /**
                     * Set up content
                     */
                    setupContent();

//                    box.hideAll();

                    break;

                case GET_RECOMMENDATION_FINISH:
                    setupAdapter();

                    //
                    break;

                case LIKE_FINISH:
                    Toast.makeText(getApplicationContext(), "Thanks for your like!", Toast.LENGTH_SHORT).show();
                    mLike.setEnabled(false);
                    break;
                case DISLIKE_FINISH:
                    mDislike.setEnabled(false);
                    break;
            }
        }
    };

    private SupportMapFragment supportMapFragment;

    /**
     * ================
     * Program starts
     * ================
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_event);

        // dynamic box
//        box = new DynamicBox(DetailEventActivity.this, R.layout.activity_detail_event);
//        box.showLoadingLayout();

        // image loader
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .build();

        ImageLoader.getInstance().init(config);

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.ic_action_reload)
                .showImageForEmptyUri(R.drawable.ic_crop_free)
                .showImageOnFail(R.drawable.ic_error_outline)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .displayer(new SimpleBitmapDisplayer())
                .build();


        // get back bundle
        // target: get type and id to find out the own event
        if (getIntent() != null) {
            Intent intent = getIntent();
            mainEvent = (Event) intent.getBundleExtra("event").getSerializable("event");
        } else {
            Log.v(TAG, "getIntent is null");
//            box.showExceptionLayout();
            onStop();
        }


        // INIT
        init();


        // parse info into completed and detailed event
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    mainEvent = eventHandler.getDetailedEvent(mainEvent);

                    if (mainEvent == null)
                        Log.v(TAG, "mainEvent is null");


                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }

                handler.sendEmptyMessage(GET_DETAILED_EVENT_FINISH);
            }
        }).start();

    }

    private void continueProcess() {
        /**
         * get totalUserEventNumber
         */

        new Thread(new Runnable() {
            @Override
            public void run() {
                // use event.author_id to get user event number
                // get user_id and call json:getUserEventNumber in userhandler to get back total events he has created.
                try {
                    totalUserEventNumber = new UserHandler(getApplicationContext()).getUserEventNumber(mainEvent.getAuthor_id());
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                handler.sendEmptyMessage(GET_DETAILS_FINISH);
            }
        }).start();

        /**
         * make bottom navigationbar
         */
        new Thread(new Runnable() {
            @Override
            public void run() {

                /**
                 * Construct bottom suggestion items
                 */

                try {

                    myDatasetArr = eventHandler.getSameTypeOf10Events(mainEvent.getEventType(), mPage_recommend);
                    // finish
                    handler.sendEmptyMessage(GET_RECOMMENDATION_FINISH);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }


    private void setupContent() {
        Log.v(TAG, "line 274");

        if (mainEvent.getLocation() != null)
            mLocation.setText(mainEvent.getLocation());
        else
            mLocation.setText("Something goes wrong. Please reload again.");


        if (mainEvent.getFrom_date() != null)
            mTime.setText(mainEvent.getFrom_date());
        else if (mainEvent.getFrom_time() != null)
            mTime.setText("No specific date " + mainEvent.getFrom_time());
        else
            mTime.setText("NOT APPLICABLE");


        if (mainEvent.getContent() != null)
            mDescription.setText(mainEvent.getContent());
        else
            mDescription.setText("Something goes wrong. Please reload again.");


        if (mainEvent.getAuthor() != null)
            mAuthor.setText(mainEvent.getAuthor());
        else
            mAuthor.setText("Something goes wrong. Please reload again.");

        // load profile pic

        ImageLoader.getInstance().displayImage(mainEvent.getProfile_pic(), mAuthorProfile, options);

        mNumberOfEventsBtn.setText("" + totalUserEventNumber + " Events");
        mNumberOfEventsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo display other events at fragment but in different activity
                Toast.makeText(getApplicationContext(), "Coming soon!", Toast.LENGTH_SHORT).show();
            }
        });


        // image slider
        makeImageSlider();
    }

    private void makeImageSlider() {
        /**
         * Load image inside Android Image Slider
         */
        Log.v(TAG, "line 155");
        DefaultSliderView sliderView;
        // if found images
        if (mainEvent.getImages().size() > 0) {

            for (int i = 0; i < mainEvent.getImages().size(); i++) {
                // init it every time
                sliderView = new DefaultSliderView(DetailEventActivity.this);
                sliderView.image(mainEvent.getImages().get(i))
                        .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                        .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                            @Override
                            public void onSliderClick(BaseSliderView slider) {
                                // display large image
                                Log.v(TAG, "line 170");

                                Log.v(TAG, "onSliderClick:" + slider.getUrl());

                                Dialog dialog = new Dialog(DetailEventActivity.this);
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog.setContentView(R.layout.image_view_hd);

                                final ImageView imageViewforHdPhotos = (ImageView) dialog.findViewById(R.id.mImageView_hd_data_detailed);
                                imageViewforHdPhotos.getLayoutParams().width = getWindowManager().getDefaultDisplay().getWidth();

                                ImageLoader.getInstance().displayImage(slider.getUrl(), imageViewforHdPhotos, options, null);

                                dialog.setCanceledOnTouchOutside(true);
                                dialog.setCancelable(true);
                                dialog.show();
                            }
                        });

                mImageSlider.addSlider(sliderView);

                Log.v(TAG, "load image into sliderview:" + mainEvent.getImages().get(i));
            }
            Log.v(TAG, "line 192");

            if (mainEvent.getImages().size() <= 1) {
                mImageSlider.stopAutoCycle();
            } else {
                mImageSlider.startAutoCycle();
            }

        } else {
            // else use cover image
            Log.v(TAG, "mainEvent.getCover_image()");

            sliderView = new DefaultSliderView(DetailEventActivity.this);
            sliderView.image(mainEvent.getCover_image())
                    .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {
                            // large image
                            Dialog dialog = new Dialog(DetailEventActivity.this);
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog.setContentView(R.layout.image_view_hd);

                            final ImageView imageViewforHdPhotos = (ImageView) dialog.findViewById(R.id.mImageView_hd_data_detailed);
                            imageViewforHdPhotos.getLayoutParams().width = getWindowManager().getDefaultDisplay().getWidth();
                            ;

                            ImageLoader.getInstance().displayImage(mainEvent.getCover_image(), imageViewforHdPhotos, options, null);
                            //imageLoader.displayImage(allImageLinks[0].replace("_thumb", ""), imageViewforHdPhotos, options, null);


                            dialog.setCanceledOnTouchOutside(true);
                            dialog.setCancelable(true);
                            dialog.show();
                        }
                    });

            mImageSlider.addSlider(sliderView);
            Log.v(TAG, "line 230");

            mImageSlider.stopAutoCycle();
        }

    }

    private void setupAdapter() {

        // make random order of data
        long seed = System.nanoTime();
        Collections.shuffle(myDatasetArr, new Random(seed));

        // setup adapter
        mAdapter = new MyRecyclerAdapter(myDatasetArr, DETAIL_RECOMMEND_VIEW) {

            @Override
            public void dynamicChangeOnLayout(RecyclerView.ViewHolder holder, final int position) {
                Log.v(TAG, " In dynamicChangeOnLayout");
                if (holder != null) {


                    DetailRecommendViewHolder drv = (DetailRecommendViewHolder) holder;

                    // like onclick that enable the alert button , hardcode
                    CardView mainCardItem = drv.mCard;
                    mainCardItem.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    // add browse record
                                    try {
                                        eventHandler.addBrowseHistory(myDatasetArr.get(position).getEventId(), preferenceManager.getEmail());
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }).start();

                            Bundle bundle = new Bundle();
                            bundle.putSerializable("event", myDatasetArr.get(position));
                            startActivity(new Intent(getApplicationContext(), DetailEventActivity.class).putExtra("event", bundle));
                        }
                    });

                    TextView mTitle, mLocation, mPrice, mPhotoNum, mPostDateTime;
                    ImageView mCover;

                    mTitle = drv.mTitle;
                    mLocation = drv.mLocation;
                    mPrice = drv.mPrice;
                    mPhotoNum = drv.mPhotoNum;
                    mCover = drv.mCover;
                    mPostDateTime = drv.mPostDateTime;

                    mTitle.setText(myDatasetArr.get(position).getName());
                    mLocation.setText(myDatasetArr.get(position).getLocation());
                    mPrice.setText(myDatasetArr.get(position).getPrice());

                    switch (myDatasetArr.get(position).getImages().size()) {
                        case 0:
                            mPhotoNum.setText("No Photo");
                            break;
                        case 1:
                            mPhotoNum.setText("1 Photo");
                            break;
                        default:
                            mPhotoNum.setText(myDatasetArr.get(position).getImages().size() + " Photos");
                            break;
                    }

                     // less useful security checking as cover image must be existed
                    if (!myDatasetArr.get(position).getCover_image().equals("null"))
                        ImageLoader.getInstance().displayImage(myDatasetArr.get(position).getCover_image(), mCover, options);

                    mPostDateTime.setText(myDatasetArr.get(position).getPost_date());

                } else
                    Log.v(TAG, "Holder is null");
            }
        };

        mDetail_recommend_recycler_view.setAdapter(mAdapter);
    }


    private void init() {

        // toolbar
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        Log.v(TAG, "the mainEvent name is " + mainEvent.getName());
        //mToolbar.setTitle(mainEvent.getName());

        // setup toolbar as actionbar
        setSupportActionBar(mToolbar);
        getSupportActionBar().setElevation(1); //set divider line
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        //collaps layout
        mCollapsing_toolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);

        //other view component
        // image slider
        mImageSlider = (SliderLayout) findViewById(R.id.androidimageslider);


        //appbar
        mAppBarLayout = (AppBarLayout) findViewById(R.id.app_bar_layout);
        mAppBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
                // i = 0 ; expand
                // i = 1 ; collapse
                if (i != 0) {
                    Log.v(TAG, "Appbar collapsed , set imageslider stop cycle");
                    mImageSlider.stopAutoCycle();
                } else if (mainEvent.getImages().size() > 1) {
                    Log.v(TAG, "Appbar expanded , start cycle");
                    mImageSlider.startAutoCycle();
                }
            }
        });

        // recommended event
        mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        mDetail_recommend_recycler_view = (RecyclerView) findViewById(R.id.detail_recommend_recycler_view);
        mDetail_recommend_recycler_view.setLayoutManager(mLayoutManager);

        // class
        eventHandler = new EventHandler(getApplicationContext());

        // content
        mLocation = (TextView) findViewById(R.id.mLocation_detail);
        mAuthor = (TextView) findViewById(R.id.mAuthorName_detail);
        mDescription = (TextView) findViewById(R.id.mDescription_detail);
        mTime = (TextView) findViewById(R.id.mTime_detail);
        mNumberOfEventsBtn = (Button) findViewById(R.id.mNumberOfEventBtn);
        mAuthorProfile = (ImageView) findViewById(R.id.profile_image);

        // BOTTOM BUTTONS
        mLike = (Button) findViewById(R.id.mBtnLike_detail);
        mLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // todo call eventHandler, pass eventid to db and add the like
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            eventHandler.giveALike(mainEvent.getEventId());

                            handler.sendEmptyMessage(LIKE_FINISH);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }
        });
        mDislike = (Button) findViewById(R.id.mDislike_detail);
        mDislike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            eventHandler.giveADislike(mainEvent.getEventId());
                            Toast.makeText(getApplicationContext(), "Oh You disliked this event!", Toast.LENGTH_SHORT).show();
                            handler.sendEmptyMessage(DISLIKE_FINISH);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                }).start();
            }
        });
        mComment = (Button) findViewById(R.id.mComment_detail); //todo not handle yet
        mShare = (Button) findViewById(R.id.mShare_detail);  // todo not handle

        preferenceManager = new SharedPreferenceManager(getApplicationContext());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detail_event, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onStop() {
        /**
         * Note! To prevent a memory leak on rotation,
         * make sure to call stopAutoCycle() on the slider before activity or fragment is destroyed:
         */
        mImageSlider.stopAutoCycle();
        super.onStop();
    }

    /**
     * =============================
     * For google map location display
     * ===============================
     */

    private GoogleMap mGoogleMap;


    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    protected void onStart() {
//        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        /* basic init */
        mGoogleMap = googleMap;

        // setup google map
        Log.v(TAG, "Map is ready, now set up camera movement and marker...");


        // move camera
        Log.v(TAG, "now move camera...");

        LatLng newGeoCode = new LatLng(Double.valueOf(mainEvent.getLat()), Double.valueOf(mainEvent.getLng()));

//        Log.v(TAG, " the latlng code is:" + newGeoCode.latitude + "," + newGeoCode.longitude);

        // 建立地圖攝影機的位置物件
        CameraPosition cameraPosition =
                new CameraPosition.Builder()
                        .target(newGeoCode)
                        .zoom(15)
                        .build();

        // 使用動畫的效果移動地圖
        mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        // 建立位置的座標物件
        mGoogleMap.addMarker(new MarkerOptions()
                .title(mainEvent.getLocation())
                .position(newGeoCode));

    }

    private void setupGoogleMap() throws IOException {

        // call onMapReady to get current location first
        supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(this);

    }
}
