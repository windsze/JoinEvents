package com.winds.joinevents;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by WindS on 25/9/2015.
 */
public class NetworkHandler {
    //todo. call this class's method in new thread every single time

    /*
    * MAIN ROLES:
    * 1/ CHECK NETWORK STATUS : ok
    * 2/ HANDLE NETWORK REQUEST
    *
    * 3/ CHECK USER EXISTENCE
    * 4/ GET ALL USER'S DETAILS INFORMATION
    * */

    //    VARIABLES
    Context context;

    OkHttpClient okHttpClient;

    private final String TAG = "NetworkHandler";

    public NetworkHandler(Context context) {

        this.context = context;
        okHttpClient = new OkHttpClient();

    }




    /*
    * It is accessable to all class that check the user's network state
    * @return whether network is connected
    * */
    public boolean isConnectedNetwork() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        if (activeNetwork != null && activeNetwork.isConnectedOrConnecting())
            return true;

        return false;
    }




}
