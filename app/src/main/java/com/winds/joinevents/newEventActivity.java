package com.winds.joinevents;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.text.format.DateFormat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.afollestad.materialdialogs.MaterialDialog;
import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialog;
import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialog;

import org.joda.time.DateTime;


public class newEventActivity extends AppCompatActivity {
    //    UI LAYOUT COMPONENTS
    // TOOLBAR
    private Toolbar mToolbar;
    private Spinner mCategoryTypeSpinner;

    // MAIN CONTENT
    private TextInputLayout mEventNameInput, mEventAddressInput, mEventOrganizationInput,
            mEventDescriptionInput, mEventPriceInput, mEventContactNoInput, mEventContactPersonInput,
            mEventFromDateTimeInput, mEventToDateTimeInput;

    private Spinner mOrganizationIDSpinner;

    private TextView mEventFrom_date, mEventFrom_time, mEventTo_date, mEventTo_time;
    private Button mBtn_create;

    //    VARIABLES
    //fragment identifier
    private static final String FRAG_TAG_TIME_PICKER = "timePickerDialogFragment";
    private static final String FRAG_TAG_DATE_PICKER = "fragment_date_picker_name";
    private int currentYear = 0, currentMonth = 0, currentDay = 0;


    //    TESTING
    private final String TAG = "newEventActivity";

    // NETWORK ISSUES
    NetworkHandler networkHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_event);

        // CHECK LOGIN STATUS, IF NOT LOGGED IN, SHOW DIALOG
        networkHandler = new NetworkHandler(this);

        if (!networkHandler.isConnectedNetwork()){
            new MaterialDialog.Builder(getApplicationContext())
                    .title(R.string.dialog_title_login)
                    .content(R.string.dialog_content_login)
                    .positiveText(R.string.dialog_positive_login)
                    .negativeText(R.string.dialog_negative_login)
                    .neutralText(R.string.dialog_neutral_login)
                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) {
                            // Todo. CALL LOGIN ACTIVITY
                            startActivity(new Intent(newEventActivity.this, loginActivity.class));
                            super.onPositive(dialog);
                        }

                        @Override
                        public void onNegative(MaterialDialog dialog) {
                            dialog.dismiss();
                            super.onNegative(dialog);
                        }

                        @Override
                        public void onNeutral(MaterialDialog dialog) {
                            // TODO. CALL WHAT LISTVIEW ACTIVITY
                            super.onNeutral(dialog);
                        }
                    })
                    .positiveText(R.string.dialog_positive_login)
                    .negativeText(R.string.dialog_negative_login)
                    .show();
        }

        // INIT
        initUI();

        // TOOLBAR
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // set up on click listener
        setupOnClickListener();


    }

    private void initUI() {


        // OTHER UI COMPONENTS
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mCategoryTypeSpinner = (Spinner) findViewById(R.id.toolbar_spin);

        mOrganizationIDSpinner = (Spinner) findViewById(R.id.organization_spinner);

        // INIT CATEGORY SPINNER
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.cateogryType_arr, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        mCategoryTypeSpinner.setAdapter(adapter);

        // INIT ORGANIZATION SPINNER
        adapter = ArrayAdapter.createFromResource(this,
                R.array.organizationId_arr, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        mOrganizationIDSpinner.setAdapter(adapter);

        // INPUTTEXTLAYOUT
        mEventNameInput = (TextInputLayout) findViewById(R.id.eventName_inputLayout);
        mEventAddressInput = (TextInputLayout) findViewById(R.id.eventAddress_inputlayout);
        mEventOrganizationInput = (TextInputLayout) findViewById(R.id.organization_inputlayout);
        mEventDescriptionInput = (TextInputLayout) findViewById(R.id.eventDescription_inputlayout);
        mEventPriceInput = (TextInputLayout) findViewById(R.id.eventPrice_input);
        mEventContactNoInput = (TextInputLayout) findViewById(R.id.eventContact_phone_input);
        mEventContactPersonInput = (TextInputLayout) findViewById(R.id.eventContact_name_input);
        mEventFromDateTimeInput = (TextInputLayout) findViewById(R.id.eventFromDateTime_input);
        mEventToDateTimeInput = (TextInputLayout) findViewById(R.id.eventToDateTime_input);

        // DATE TIME TEXTVIEW
        mEventFrom_date = (TextView) findViewById(R.id.register_birthday);
        mEventFrom_time = (TextView) findViewById(R.id.eventFrom_time);
        mEventTo_date = (TextView) findViewById(R.id.eventTo_date);
        mEventTo_time = (TextView) findViewById(R.id.eventTo_time);

        // BUTTON
        mBtn_create = (Button) findViewById(R.id.mBtn_create);

    }

    DateTime now;
    private void setupOnClickListener() {
        now = DateTime.now();

        currentYear = now.getYear();
        currentMonth = now.getMonthOfYear();
        currentDay = now.getDayOfMonth();


        // date from - onClick
        mEventFrom_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CalendarDatePickerDialog calendarDatePickerDialog = CalendarDatePickerDialog
                        .newInstance(new CalendarDatePickerDialog.OnDateSetListener() {
                                         @Override
                                         public void onDateSet(CalendarDatePickerDialog calendarDatePickerDialog, int year, int monthOfYear, int dayOfMonth) {
                                             mEventFrom_date.setText(dayOfMonth + "/" + monthOfYear + "/" + year);
                                         }
                                     }, now.getYear(), now.getMonthOfYear() - 1,
                                now.getDayOfMonth());
                calendarDatePickerDialog.show(getSupportFragmentManager(), FRAG_TAG_DATE_PICKER);
            }
        });
        // time from - onclick
        mEventFrom_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateTime now = DateTime.now();
                RadialTimePickerDialog timePickerDialog = RadialTimePickerDialog
                        .newInstance(new RadialTimePickerDialog.OnTimeSetListener() {
                                         @Override
                                         public void onTimeSet(RadialTimePickerDialog radialTimePickerDialog, int hourOfDay, int minute) {
                                             if (hourOfDay >= 12) {
                                                 // pm
                                                 mEventFrom_time.setText(hourOfDay - 12 + ":" + minute + " PM");
                                             } else {
                                                 // am
                                                 mEventFrom_time.setText(hourOfDay + ":" + minute + " AM");
                                             }
                                         }
                                     }, now.getHourOfDay(), now.getMinuteOfHour(),
                                DateFormat.is24HourFormat(getApplicationContext()));

                Log.v(TAG, "test1");
                timePickerDialog.show(getSupportFragmentManager(), FRAG_TAG_TIME_PICKER);


            }
        });

        // date to - onclikc
        mEventTo_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateTime now = DateTime.now();
                CalendarDatePickerDialog calendarDatePickerDialog = CalendarDatePickerDialog
                        .newInstance(new CalendarDatePickerDialog.OnDateSetListener() {
                                         @Override
                                         public void onDateSet(CalendarDatePickerDialog calendarDatePickerDialog, int year, int monthOfYear, int dayOfMonth) {
                                            // monthOfYear by default is less 1
                                             monthOfYear = monthOfYear+1;

                                             // cannot be the past event
                                             if (year < currentYear || monthOfYear < currentMonth || dayOfMonth < currentDay) {
                                                 // is past event
                                                 Log.v(TAG,"current Year: "+currentYear+"\ncurrent month: "+currentMonth+"\ncurrent day: "+currentDay);
                                                 Log.v(TAG,"year: "+year+"\nmonth: "+monthOfYear+1 +"\nday: "+dayOfMonth);
                                                 Toast.makeText(getApplicationContext(), "You cannot create a past event", Toast.LENGTH_SHORT).show();
                                             } else {
                                                 mEventTo_date.setText(dayOfMonth + "/" + monthOfYear + "/" + year);
                                             }
                                         }
                                     }, now.getYear(), now.getMonthOfYear() - 1,
                                now.getDayOfMonth());
                calendarDatePickerDialog.show(getSupportFragmentManager(), FRAG_TAG_DATE_PICKER);
            }
        });

        // time to - onclick
        mEventTo_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateTime now = DateTime.now();
                RadialTimePickerDialog timePickerDialog = RadialTimePickerDialog
                        .newInstance(new RadialTimePickerDialog.OnTimeSetListener() {
                                         @Override
                                         public void onTimeSet(RadialTimePickerDialog radialTimePickerDialog, int hourOfDay, int minute) {
                                             if (hourOfDay >= 12) {
                                                 // pm
                                                 mEventTo_time.setText(hourOfDay - 12 + ":" + minute + " PM");
                                             } else {
                                                 // am
                                                 mEventTo_time.setText(hourOfDay + ":" + minute + " AM");
                                             }
                                         }
                                     }, now.getHourOfDay(), now.getMinuteOfHour(),
                                DateFormat.is24HourFormat(newEventActivity.this));

                timePickerDialog.show(getSupportFragmentManager(), FRAG_TAG_TIME_PICKER);


            }
        });

        mBtn_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // check whether all infos are filled in
                if (mEventNameInput.getEditText().length() == 0) {
                    mEventNameInput.setError(getResources().getString(R.string.error_eventName));
                }
                // address
                else if (mEventAddressInput.getEditText().length() == 0) {
                    mEventAddressInput.setError(getResources().getString(R.string.eroor_eventAdress));
                }
                // organ
                else if (mOrganizationIDSpinner.getId() == 0) {
                    mEventOrganizationInput.setError(getResources().getString(R.string.error_eventOrgan));
                }
                // description
                else if (mEventDescriptionInput.getEditText().length() == 0) {
                    mEventDescriptionInput.setError(getResources().getString(R.string.error_eventDescription));
                }
                // price
                else if (mEventPriceInput.getEditText().length() == 0) {
                    mEventPriceInput.setError(getResources().getString(R.string.error_eventPrice));
                }
                // price not numerical
                else if (!isNumeric(mEventPriceInput.getEditText().getText().toString())) {
                    mEventPriceInput.setError(getResources().getString(R.string.error_eventPrice_nonNum));
                }
                // from date and time
                else if (mEventFrom_date.getText().length() == 0 ||
                        mEventFrom_time.getText().length() == 0) {
                    mEventFromDateTimeInput.setError(getResources().getString(R.string.error_date_time_general));
                }
                // to date and time
                else if (mEventTo_date.getText().length() == 0 ||
                        mEventTo_time.getText().length() == 0) {
                    mEventToDateTimeInput.setError(getResources().getString(R.string.error_date_time_general));
                }
                // contact no
                else if (mEventContactNoInput.getEditText().length() == 0) {
                    mEventContactNoInput.setError(getResources().getString(R.string.error_contact_no));
                }
                // contact person
                else if (mEventContactPersonInput.getEditText().length() == 0) {
                    mEventContactPersonInput.setError(getResources().getString(R.string.error_contact_person));
                }
                // no problem
                else {


                    //show a list of all details for reconfirm
                    new MaterialDialog.Builder(getApplicationContext())
                            .title(R.string.dialog_title_newEvent)
                            .content(R.string.dialog_content_newEvent) //??
                            .positiveText(R.string.dialog_agree_newEvent)
                            .callback(new MaterialDialog.ButtonCallback() {
                                @Override
                                public void onPositive(MaterialDialog dialog) {
                                    super.onPositive(dialog);

                                    // do confirmed
                                    // first check whether have network
                                    NetworkHandler networkHandler = new NetworkHandler(getApplicationContext());

                                    // if yes, connect db by calling controller
                                    if (networkHandler.isConnectedNetwork()) {
                                        // INSERT ALL CONTENTS INTO VARIABLES
                                        Event finishEvent = createEvent();

                                        // pass to eventHandler to write into db
                                        new EventHandler(getApplicationContext()).createEvent(finishEvent);
                                        
                                    }else{
                                        //if no, prompt user
                                        Toast.makeText(getApplicationContext(), "Please connect internet",Toast.LENGTH_SHORT).show();
                                    }
                                }
                            })
                            .negativeText(R.string.dialog_disagree_newEvent)
                            .callback(new MaterialDialog.ButtonCallback() {
                                @Override
                                public void onNegative(MaterialDialog dialog) {
                                    super.onNegative(dialog);

                                    // cancel event : do nothing
                                }
                            })
                            .show();



                }


            }
        });

    }

    private Event createEvent() {
        Event event = new Event();

        event.setEventType(mCategoryTypeSpinner.getId());
        event.setName(mEventNameInput.getEditText().toString());
//        event.setOrganization_id(mOrganizationIDSpinner.getId());
        event.setFrom_time(mEventFrom_time.getText().toString());
        event.setFrom_date(mEventFrom_date.getText().toString());
        event.setTo_time(mEventTo_time.getText().toString());
        event.setTo_date(mEventTo_date.getText().toString());
        event.setLocation(mEventAddressInput.getEditText().toString());
        event.setContactNo(mEventContactNoInput.getEditText().toString());
        event.setContactPerson(mEventContactPersonInput.getEditText().toString());
//        event.setDescription_detail(mEventDescriptionInput.getEditText().toString());
        event.setPrice(mEventPriceInput.getEditText().toString());

        return event;

    }

    private boolean isNumeric(String str) {
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_new_event, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_save:
                return true;
            case R.id.action_settings:
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
