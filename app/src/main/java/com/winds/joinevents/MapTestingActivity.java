package com.winds.joinevents;

import android.app.Notification;
import android.location.Location;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.location.LocationListener;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;

import br.com.goncalves.pugnotification.notification.PugNotification;


public class MapTestingActivity extends ActionBarActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    // flag
    private static final String TAG = "MapTestingActivity";

    // ui
    EditText mLocation;
    Button mSubmit;
    SupportMapFragment supportMapFragment;
    TextView mDebugWindows;
    private GoogleMap mGoogleMap;

    // dummi event
    Event dummiEvent;
    LatLng dummiEvent_latlng;

    // current
    private LatLng dummiCurrentLocation;
    private LatLng currentLatLng;
    LocationQueryHandler locationServiceHandler;
    private Location mLastLocation;

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest locationrequest;


    // Constant
    private static final long INTERVAL = 1000 * 20;
    private static final long FASTEST_INTERVAL = 1000 * 60 * 5; //  5 minutes location updates


    /**
     * Objective:
     * 1. when launch activity, the textview will show the lat lng location of current point V
     * 2 can be used in creating events & events location tracking
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_testing);

        // show error dialog if GoolglePlayServices not available
        if (!isGooglePlayServicesAvailable()) {
            finish();
        }

        // create instance of locationrequest
        createLocationRequest();

        // Create instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            Log.v(TAG, " building mGoogleApiClient");
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        // call onMapReady to get current location first
        supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(this);

        // init UI
        mDebugWindows = (TextView) findViewById(R.id.mDebugWindows);
        locationServiceHandler = new LocationQueryHandler(getApplicationContext());

//        // make dummi event
//        dummiEvent = new Event();
//        LatLng tempLatLng = null;
//        try {
//            tempLatLng = locationServiceHandler.getGeocoding("Hong Kong Baptist University");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        dummiEvent.setLocation("" + tempLatLng.latitude + "," + tempLatLng.longitude);
//        dummiEvent.setName("Dummi Event: Hong Kong Baptist University");
//        dummiEvent_latlng = new LatLng(tempLatLng.latitude, tempLatLng.longitude);

        // ui cont'd
        mLocation = (EditText) findViewById(R.id.mLocation);
        mSubmit = (Button) findViewById(R.id.mSubmit);
        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addDebug("Submit onclick");
                // check the text and get latlng object back
                if (mLocation.getText().toString().length() != 0) {
                    try {

//                        LatLng newGeoCode = locationServiceHandler.getGeocoding(mLocation.getText().toString());
//                        addDebug(" the latlng code is:" + newGeoCode.latitude + "," + newGeoCode.longitude);
//
//                        if (newGeoCode != null) {
//                            mLocation.setText(locationServiceHandler.getOneAddress(newGeoCode));
//                            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(newGeoCode, 18));
//                            mGoogleMap.addMarker(new MarkerOptions()
//                                    .title(mLocation.getText().toString())
//                                    .position(newGeoCode));
//                        }

                        // testing notification - input any location and make comparison between
                        LatLng inputLocationLatLng = locationServiceHandler.getGeocoding(mLocation.getText().toString());
                        // inputLocationLatLng can be null;
                        if (inputLocationLatLng != null) {
                            addDebug("inputLocation is: " + mLocation.getText().toString());
                            addDebug("inputLocation latlng is: " + inputLocationLatLng.toString());
                            addDebug("inputLocation in Google API is:" + locationServiceHandler.getOneAddress(inputLocationLatLng));

                            // distance between two geolocation
                            if (mLastLocation == null) {
                                Log.w(TAG, "mLastLocation is not ready! ");
                            } else {

                                double distanceBetween = locationServiceHandler.getDistance(inputLocationLatLng, new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()));
                                addDebug("distanceBetween is:" + distanceBetween);
                                if (distanceBetween < 500) {
                                    makeNotification(distanceBetween);
                                }
                            }
                        } else
                            addDebug("Addresses from google API is null");

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }
        });


    }


    private void makeNotification(double distanceBetween) {
        PugNotification.with(getApplicationContext())
                .load()
                .title(mLocation.getText().toString())
                .message("You are close to " + mLocation.getText().toString() + "in " + distanceBetween + " meters")
                .smallIcon(R.mipmap.ic_launcher)
                .largeIcon(R.mipmap.ic_launcher) //
                .flags(Notification.DEFAULT_ALL)
                .simple()
                .build();
    }

    private void createLocationRequest() {
        locationrequest = new LocationRequest();
        locationrequest.setInterval(INTERVAL);
        locationrequest.setFastestInterval(FASTEST_INTERVAL);
        locationrequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }


    @Override
    public void onMapReady(final GoogleMap googleMap) {

        /* basic init */
        mGoogleMap = googleMap;
        mGoogleMap.setMyLocationEnabled(true);
        mGoogleMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                try {
                    updateLocation();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return true;
            }
        });


        //currentLocationLatLng = new LatLng(mGoogleMap.getMyLocation().getLatitude(), mGoogleMap.getMyLocation().getLongitude());


        /* Main Programming */

        /*try {
            // get a example latlng
            hkbu_geocoding = locationServerHandler.getGeocoding("HONG KONG BAPTIST UNIVERSITY");
            addDebug("HKBU's location latlng is: "+hkbu_geocoding.latitude+" , "+hkbu_geocoding.longitude);

//            // get current location latlng
//            addDebug("now get current location latlng...");
//            currentLocationLatLng = new LatLng(
//                    getMyLocation().getLatitude(),
//                    googleMap.getMyLocation().getLongitude()
//            );

//            addDebug("current location latlng is: "+currentLocationLatLng.latitude+" , "+currentLocationLatLng.longitude);

            addDebug("the distance between is: " + locationServerHandler.getDistance(hkbu_geocoding, currentLocationLatLng) + " m");

            while(true){
                // check if is moving
                if (locationServerHandler.isMoving()){
                    // get the geocode
                    locationServerHandler.getCurrentGeocoding(mGoogleMap);

                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }


    @Override
    public void onConnected(Bundle bundle) {
        Log.v(TAG, "in onConnected");

        // start location updates
        startLocationUpdateProcess();

    }

    private void startLocationUpdateProcess() {
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, locationrequest, this);
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        try {
            updateLocation();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updateLocation() throws IOException {
        // update location information into debug textview
        if (mLastLocation != null) {
//            Log.v(TAG, "mLastLocation != null");
            currentLatLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());

            Log.v(TAG, "Update location: Address: " + locationServiceHandler.getOneAddress(currentLatLng));
            Log.v(TAG, "Update location: last update time: " + DateFormat.getTimeInstance().format(new Date()));

            // move camera after updates
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 17));


            mGoogleMap.addMarker(new MarkerOptions()
                    //.draggable(true)
                    .position(currentLatLng));

            mGoogleMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                @Override
                public void onMarkerDragStart(Marker marker) {

                }

                @Override
                public void onMarkerDrag(Marker marker) {

                }

                @Override
                public void onMarkerDragEnd(Marker marker) {
//                currentLatLng = marker.getPosition();
//                addDebug("onMarkerDragEnd: latLng is:" + currentLatLng.latitude + "," + currentLatLng.longitude);
//                String location = locationServiceHandler.getOneAddress(currentLatLng);
//                addDebug("onMarkerDragEnd: location is:" + location);
//                mLocation.setText(location);
//
//                // cal the distance between - meters - Straight line distance
//                double distanceBtw = locationServiceHandler.getDistance(currentLatLng, currentLatLng);
//                addDebug("distance between:" + distanceBtw + "meters");

                }
            });
        } else {
            Log.v(TAG, "mLastLocation == null");
        }
    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }


    @Override
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mGoogleApiClient.isConnected()) {
            startLocationUpdateProcess();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        stopLocationUpdates();
    }

    protected void stopLocationUpdates() {

        if (mGoogleApiClient.isConnected())
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_map_testing, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, this, 0).show();
            return false;
        }
    }


    private void addDebug(String msg) {
        mDebugWindows.append("\n" + msg);
        mDebugWindows.computeScroll();
        Log.w(TAG, msg);
    }
}
