package com.winds.joinevents;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.prototypes.SwipeDismissListItemViewTouchListener;
import it.gmariotti.cardslib.library.recyclerview.view.CardRecyclerView;
import mehdi.sakout.dynamicbox.DynamicBox;

public class MyFavoriteActivity extends AppCompatActivity {

    private static final String TAG = "MyFavoriteActivity";
    private static final int FINISH_PARSE_EVENTS = 0;
    private ArrayList<Event> list;

    //UI
    private TextView mTitle, mLocation, mAuthor, mType, mAddTime;
    private ImageView mCover_pic;
    private CardRecyclerView mRecyclerView;
    private RelativeLayout mItemView;
    private MyRecyclerAdapter mAdapter;
    private ArrayList<Event> cards;
    private Toolbar mToolbar;
    //    private ImageButton mAlertBtn;
    private CheckBox mAlertCheck;

    //Class
    private EventHandler mEventHandler;
    private SharedPreferenceManager preferenceManager;
    private NotificationHandler notificationHandler;

    // FLAG
    private static final int LIST_RECYCLER_VIEW = 0;
    private static int ALERT_BTN_COUNT = 2;
    private int mPage = 1;

    // image loader
    ImageLoader imageLoader;
    DisplayImageOptions options;

    //
    List<Integer> eventIdList;

    // for detection of human click checkbox but not system make changes
    boolean humanClicked = false;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case FINISH_PARSE_EVENTS:

                    Log.w(TAG, "in FINISH_PARSE_EVENTS");

                    if (list.size() != 0) {
                        findViewById(R.id.mNoContent_myfavorite).setVisibility(View.INVISIBLE);
                        Log.w(TAG, "list.size() > 0");
                    }

                    // get back the event's id in sqlite for receive notification
                    eventIdList = notificationHandler.getEventIdList();

                    mAdapter = new MyRecyclerAdapter(list, LIST_RECYCLER_VIEW) {

                        @Override
                        public void dynamicChangeOnLayout(final RecyclerView.ViewHolder holder, final int position) {
                            Log.v(TAG, " In dynamicChangeOnLayout");
                            if (holder != null) {
                                MyFavoriteListViewHolder lvh = (MyFavoriteListViewHolder) holder;

                                mItemView = lvh.mItemView;
                                mTitle = lvh.mTitle;
                                mAuthor = lvh.mAuthor;
                                mCover_pic = lvh.mCover;
                                mLocation = lvh.mLocation;
                                mType = lvh.mType;
                                mAddTime = lvh.mAddBookmarkDateTime;
//                                mAlertBtn = lvh.mAlarmBtn_ImageBtn;
                                mAlertCheck = lvh.mAlertCheck;


                                // add content
                                assignContentForList(position);

                            } else
                                Log.v(TAG, "Holder is null");
                        }
                    };
                    mRecyclerView.setAdapter(mAdapter);


                    break;
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myfavorite);

        InitUIComponents();

        // setup toolbar as actionbar
        setSupportActionBar(mToolbar);
        getSupportActionBar().setElevation(1); //set divider line
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //set up adapter for recyclerview
        // use a linear layout manager
        mRecyclerView = (CardRecyclerView) findViewById(R.id.mRecyclerView_myFavorite);
        mRecyclerView.addItemDecoration(new SimpleItemDividerDecoration(getResources()));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        // get all bookmarks for this user
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    list = mEventHandler.get10FavoriteEvents(mPage, preferenceManager.getEmail());
                    Log.w(TAG, "in runnable, list size is:" + list.size());
                    mHandler.sendEmptyMessage(FINISH_PARSE_EVENTS);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void InitUIComponents() {
        //UI
        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        // VARIABLE
        mEventHandler = new EventHandler(getApplicationContext());

        // class
        preferenceManager = new SharedPreferenceManager(getApplicationContext());

        // image loader
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .build();

        ImageLoader.getInstance().init(config);

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.ic_action_reload)
                .showImageForEmptyUri(R.drawable.ic_crop_free)
                .showImageOnFail(R.drawable.ic_error_outline)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .displayer(new SimpleBitmapDisplayer())
                .build();

        //todo all onclick listener
//
//        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
//
//            @Override
//            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
//                return false;
//            }
//
//            @Override
//            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
//                //Remove swiped item from list and notify the RecyclerView
//                Log.w(TAG, "on onSwiped");
//                if (swipeDir == ItemTouchHelper.LEFT){
////                    mAdapter.onItemDismiss(viewHolder.getAdapterPosition());
//                    Log.w(TAG, "ItemTouchHelper.LEFT");
//                }else if (swipeDir == ItemTouchHelper.RIGHT)
//                    Log.w(TAG, "ItemTouchHelper.Right");
//            }
//        };
//
//        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
//        itemTouchHelper.attachToRecyclerView(mRecyclerView);
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder1) {
                Log.w(TAG, "onMove");
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                Log.w(TAG, "onSwiped");
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(mRecyclerView);

        notificationHandler = new NotificationHandler(getApplicationContext());


    }

    private void assignContentForList(final int position) {

        mTitle.setText(list.get(position).getName());
        mLocation.setText(list.get(position).getLocation());
        mAuthor.setText(list.get(position).getAuthor());
        mType.setText(list.get(position).getEventTypeName());
        mAddTime.setText(list.get(position).getBookmark_time());

        // use imageLoader for loading the image
        ImageLoader.getInstance().displayImage(list.get(position).getCover_image(), mCover_pic, options);


        // default
        mAlertCheck.setChecked(false);

        // check event id is in db
        if (eventIdList.size() > 0) {

            loop:
            for (int id : eventIdList) {
                Log.e(TAG, "id in eventIdList is: " + id);
                Log.e(TAG, "id in eventList is" + list.get(position).getEventId());

                // check for on case
                if (list.get(position).getEventId() == id) {
                    mAlertCheck.setChecked(true);

                    break loop;
                }
            }
        }

        // oncheck listener
        mAlertCheck.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                humanClicked = true;

                return false;
            }
        });

        mAlertCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked && humanClicked) {
                    // receive notification
                    notificationHandler.addEvent(list.get(position));
                    Log.e(TAG, "add event to sqlite finish");
                    Toast.makeText(getApplicationContext(), "已開啟通知 - 活動 " + list.get(position).getName(), Toast.LENGTH_SHORT).show();
                } else if (!isChecked && humanClicked){
                    notificationHandler.deleteEvent(list.get(position));
                    Log.e(TAG, "delete event from sqlite finish");
                    Toast.makeText(getApplicationContext(), "已關閉通知 - 活動 " + list.get(position).getName(), Toast.LENGTH_SHORT).show();
                }

                humanClicked = false;
            }
        });

        mItemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        // add browse record
                        try {
                            mEventHandler.addBrowseHistory(list.get(position).getEventId(),preferenceManager.getEmail());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();

                Bundle bundle = new Bundle();
                bundle.putSerializable("event", list.get(position));
                startActivity(new Intent(getApplicationContext(), DetailEventActivity.class).putExtra("event", bundle));
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my_favoriate, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
