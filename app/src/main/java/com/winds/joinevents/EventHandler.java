package com.winds.joinevents;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.EventLog;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by WindS on 8/10/2015.
 */
public class EventHandler {
    private static final String TAG = "EventHandler";
    OkHttpClient okHttpClient;
    Context context;

    public EventHandler(Context context) {
        this.context = context;
    }


    public boolean createEvent(Event event) {
        //todo pass the event content in GET method --> need to change the php file into GET first
        // havnt correct newEvent.php


        // if request.response.isSuccessful, return true;
        return false;

    }

    public boolean modifyEvent(Event event) {
        //todo pass the event content in GET method --> need to change the php file into GET first
        // if request.response.isSuccessful, return true;
        return false;

    }

    public boolean deleteEvent(Event event) {
        //todo pass the event content in GET method --> need to change the php file into GET first
        // if request.response.isSuccessful, return true;
        return false;

    }

    /**
     * Get 10 events from db with int page as index for different page
     *
     * @param page
     * @return ArrayList<Event>
     * @throws IOException
     */
    public ArrayList<Event> get10EventsList_card(final int page) throws IOException {
        /*
        * done
        * get all event items from db
        * pass all items into arraylist
        * */
        ArrayList<Event> events;
        okHttpClient = new OkHttpClient();

        Log.v(TAG, "url is:" + "http://join-events.co.nf/events/json_get10Events_card.php?page=" + page);
        Request request = new Request.Builder()
                .url("http://join-events.co.nf/events/json_get10Events_card.php?page=" + page)
                .build();
        Response response = okHttpClient.newCall(request).execute();

        String eventJsonResponse = response.body().string();

        Log.v(TAG, "Event Json:" + eventJsonResponse);

        if (eventJsonResponse.length() > 2) // empty json will be "[]"
            events = new JSONHandler().parseEventsList_card(eventJsonResponse);
        else
            events = new ArrayList<>(); // empty arrayList

        return events;
    }

    /**
     * Get event with full details for the uage of detailed event page
     *
     * @param eventFromCard a event containing only parts of info in card page.
     * @return null for unexpected case, return completed info event normally
     * @throws IOException
     * @throws JSONException
     */
    public Event getDetailedEvent(Event eventFromCard) throws IOException, JSONException {
        int type = eventFromCard.getEventType();
        int id = eventFromCard.getEventId();
        Event completeEvent = null;


        // flag
        final int LEISURE_EVENT = 1;
        final int FOOD_EVENT = 2;

        okHttpClient = new OkHttpClient();

//        switch (type) {
//            case LEISURE_EVENT:
//
//                Log.v(TAG, "url is:" + "http://join-events.co.nf/events/json_getDetailedEvent.php?id=" + id);
//                Request request = new Request.Builder()
//                        .url("http://join-events.co.nf/events/json_getDetailedEvent.php?id=" + id)
//                        .build();
//                Response response = okHttpClient.newCall(request).execute();
//
//                String eventJsonResponse = response.body().string();
//
//                Log.v(TAG, "Event Json:" + eventJsonResponse);
//
//                completeEvent = new JSONHandler().parseDetailedEvent(eventFromCard, eventJsonResponse);
//
//                break;
//            case FOOD_EVENT:
//                completeEvent = null;
//                //todo parse info inside
//                break;
//        }
        /**
         * dont handle different event's type first. all are the same
         */
        Log.v(TAG, "url is:" + "http://join-events.co.nf/events/json_getDetailedEvent.php?id=" + id);
        Request request = new Request.Builder()
                .url("http://join-events.co.nf/events/json_getDetailedEvent.php?id=" + id)
                .build();
        Response response = okHttpClient.newCall(request).execute();

        String eventJsonResponse = response.body().string();

        Log.v(TAG, "Event Json:" + eventJsonResponse);

        completeEvent = new JSONHandler().parseDetailedEvent(eventFromCard, eventJsonResponse);

        return completeEvent;

    }

    /**
     * It helps connect to db and save a specific event (using event_id) with user_id to bookmark table
     *
     * @param event_id   specify particular event
     * @param user_email save to which user's favorite page by s/he's unique email
     * @return success or fail state
     * @throws IOException
     */
    public boolean saveToMyFavoritePage(int event_id, String user_email) throws IOException {

        Request request = new Request.Builder()
                .url("http://join-events.co.nf/bookmark/post_newBookmark.php?" +
                                "event_id=" + event_id + "&" +
                                "email=" + user_email
                )
                .build();
        Response response = okHttpClient.newCall(request).execute();


        String responseStr = response.body().string();

        Log.v(TAG, "response is " + responseStr);

        // false1062 = duplicated key -> have record already
        if (responseStr.startsWith("false"))
            return false;

        return true;
    }


    public ArrayList<Event> get10FavoriteEvents(final int page, String user_email) throws IOException {
        /*
        * //done
        * get all event items from db
        * pass all items into arraylist
        * */
        ArrayList<Event> events;
        okHttpClient = new OkHttpClient();

//        Log.v(TAG, "get10FavoriteEvents: url is:" + "http://join-events.co.nf/events/json_get10FavoriteEvents_card.php?page=" + page + "&user_id=" + user_id);
        Request request = new Request.Builder()
                .url(("http://join-events.co.nf/events/json_get10BookmarkEvents_card.php?" +
                        "email=" + user_email + "& " +
                        "page=" + page))
                .build();
        Response response = okHttpClient.newCall(request).execute();

        String eventJsonResponse = response.body().string();

        Log.v(TAG, "Event Json:" + eventJsonResponse);

        if (eventJsonResponse.length() > 2) // empty json will be "[]"
            events = new JSONHandler().parseBookmarkEventsList_card(eventJsonResponse);
        else {
            events = new ArrayList<>(); // empty arrayList

            Log.e(TAG, "empty events array list has been assigned in get10FavoriteEvents");
        }

        return events;
    }

    public ArrayList<Event> getSameTypeOf10Events(int type, int page) throws IOException {
        /*
        * //done
        * get all event items from db
        * pass all items into arraylist
        * */
        ArrayList<Event> events;
        okHttpClient = new OkHttpClient();

//        Log.v(TAG, "get10FavoriteEvents: url is:" + "http://join-events.co.nf/events/json_get10FavoriteEvents_card.php?page=" + page + "&user_id=" + user_id);
        Request request = new Request.Builder()
                .url(("http://join-events.co.nf/events/json_getSameTypeOf10Events.php?" +
                        "type=" + type + "& " +
                        "page=" + page))
                .build();
        Response response = okHttpClient.newCall(request).execute();

        String eventJsonResponse = response.body().string();

        Log.v(TAG, "Event Json:" + eventJsonResponse);

        if (eventJsonResponse.length() > 2) // empty json will be "[]"
            events = new JSONHandler().parseEventsList_card(eventJsonResponse);
        else
            events = new ArrayList<>(); // empty arrayList

        return events;
    }

    /**
     * @param type       -1 or positive number
     * @param page       1 or above
     * @param price_from 0 or above
     * @param price_to
     * @param location   String or null
     * @return Event array list
     * @throws IOException
     */
    public ArrayList<Event> get10FilterEvents(int type, int page, int price_from, int price_to, String location) throws IOException {
        /*
        * //done
        * get all event items from db
        * pass all items into arraylist
        * */
        ArrayList<Event> events;
        okHttpClient = new OkHttpClient();

        // predefine
        String url = "http://join-events.co.nf/events/json_get10FilterEvents.php?" + "page=" + page;

        // add location parameter
        if (location != null) {
            url = url + "&location=" + location;
        } else {
            url = url + "&location=";
        }

        // add price parameter
        url = url + "&price_to=" + price_to;
        url = url + "&price_from=" + price_from;

        // add type parameter
        url = url + "&type=" + type;


        Log.e(TAG, "url is:" + url);

//        Log.v(TAG, "get10FavoriteEvents: url is:" + "http://join-events.co.nf/events/json_get10FavoriteEvents_card.php?page=" + page + "&user_id=" + user_id);

        Request request = new Request.Builder()
                .url(url)
                .build();
        Response response = okHttpClient.newCall(request).execute();

        String eventJsonResponse = response.body().string();

        Log.v(TAG, "Event Json:" + eventJsonResponse);

        if (eventJsonResponse.length() > 2) // empty json will be "[]"
            events = new JSONHandler().parseEventsList_card(eventJsonResponse);
        else
            events = new ArrayList<>(); // empty arrayList

        return events;
    }


    public boolean giveALike(int event_id) throws IOException {
        Request request = new Request.Builder()
                .url("http://join-events.co.nf/events/post_giveALike.php?" +
                                "event_id=" + event_id
                )
                .build();
        Response response = okHttpClient.newCall(request).execute();


        String responseStr = response.body().string();

        Log.v(TAG, "response is " + responseStr);

        // false1062 = duplicated key -> have record already
        if (responseStr.startsWith("false"))
            return false;

        return true;
    }

    public boolean giveADislike(int event_id) throws IOException {
        Request request = new Request.Builder()
                .url("http://join-events.co.nf/events/post_giveADislike.php?" +
                                "event_id=" + event_id
                )
                .build();
        Response response = okHttpClient.newCall(request).execute();


        String responseStr = response.body().string();

        Log.v(TAG, "response is " + responseStr);

        // false1062 = duplicated key -> have record already
        if (responseStr.startsWith("false"))
            return false;

        return true;
    }

    public void addBrowseHistory(int event_id, String user_email) throws IOException {
//        String url = "http://join-events.co.nf/users/post_updateBrowseHistory.php?email=s.friday1004@gmail.com&event_id=2";

        Request request = new Request.Builder()
                .url("http://join-events.co.nf/users/post_updateBrowseHistory.php?" +
                                "email=" + user_email + "&" +
                                "event_id=" + event_id
                )
                .build();

        Response response = okHttpClient.newCall(request).execute();

        // check querying db status
        if (!response.isSuccessful())
            throw new IOException("Unexpected code " + response);
        else
            Log.v(TAG, response.body().string());

    }


    /*
    * In createEvent method, it will call postFile (pass file selected by user) to post the file to the php
    * @param File that need to be passed to the server
    * @return success state
    * */
    private boolean postFile(File postThisFile) {
        //todo https://github.com/square/okhttp/wiki/Recipes#posting-a-file
//        Bitmap image = postThisFile.get
        return false;
    }

    //todo hardcode and ignore first, see below real code
    public ArrayList<Event> getNear10Events(LatLng currentLocation, int page) throws IOException {
        return null;
    }

//    public ArrayList<Event> getNear10Events(LatLng currentLocation, int mPage) throws IOException {
//        Request request = new Request.Builder()
//                .url("http://join-events.co.nf/users/.php?" +
//                        "email=" + user_email + "&" +
//                        "event_id=" + event_id
//                )
//                .build();
//
//        Response response = okHttpClient.newCall(request).execute();
//
//        // check querying db status
//        if (!response.isSuccessful())
//            throw new IOException("Unexpected code " + response);
//        else
//            Log.v(TAG, response.body().string());
//    }
}
