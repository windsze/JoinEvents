package com.winds.joinevents;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.hardware.SensorEvent;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;

import com.google.android.gms.location.DetectedActivity;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * This class handles all location related service,
 * for example, convert geocode to address or convert
 * address to geocode.
 * It also checks for whether user is moving and
 * cal the distance between two points of address.
 */
public class LocationQueryHandler {
    private static final String TAG = "LocationServerHandler";
    Context context;

    boolean isMoving = false;

    public static final String PACKAGE_NAME = "com.winds.joinevents";

    public static final String BROADCAST_ACTION = PACKAGE_NAME + ".BROADCAST_ACTION";

    /**
     * Constructor
     */

    public LocationQueryHandler(Context context) {
        this.context = context;
    }

    /**
     * Get first address from list of address by using Lat and Lng object
     *
     * @param addressLatLng
     * @return String - Address Name String
     * @throws IOException
     */

    String[] addresses;

    public String getOneAddress(LatLng addressLatLng) {

//        List<Address> lstAddress = gc.getFromLocation(addressLatLng.latitude, addressLatLng.longitude, 10);
        final OkHttpClient okhttpclient = new OkHttpClient();

        final Request request = new Request.Builder()
                .url("http://maps.google.com/maps/api/geocode/json?latlng=" + addressLatLng.latitude + "," + addressLatLng.longitude + "&sensor=false")
                .addHeader("Accept-Language", "zh-TW,zh;q=0.8,en-US;q=0.5,en;q=0.3")
                .build();

//        System.out.println(request.urlString());
        // parse response from url to jsonString with UTF-8 formatting

        addresses = new String[0];
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Response response = okhttpclient.newCall(request).execute();
                    String responseString = new String(response.body().string().getBytes("UTF-8"));
                    addresses = new JSONHandler().parseJsonToAddress(responseString);

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // return address
        if (addresses.length != 0) {
            return addresses[0];
        }

        // empty address found
        Log.w(TAG, "Empty addresses from Google API found");
        return null;
    }

    /**
     * Get address's latlng value
     *
     * @param addressName
     * @return
     * @throws IOException
     */
    public LatLng getGeocoding(String addressName) throws IOException {
        Geocoder geocoder = new Geocoder(context);

        List<Address> lstAddress = geocoder.getFromLocationName(addressName, 1);

        // can be empty for empty result
        if (lstAddress.isEmpty())
            return null;
        else {
            double lat = lstAddress.get(0).getLatitude();
            double lng = lstAddress.get(0).getLongitude();

            return new LatLng(lat, lng);
        }

    }

    public LatLng getCurrentGeocoding(GoogleMap currentMap) {
        LatLng currentLatLng = new LatLng(
                currentMap.getMyLocation().getLatitude(),
                currentMap.getMyLocation().getLongitude()
        );

        return currentLatLng;
    }

    /**
     * from http://blog.csdn.net/mad1989/article/details/9933089
     * 计算两点之间距离
     *
     * @param start
     * @param end
     * @return 米
     */
    public double getDistance(LatLng start, LatLng end) {
        double lat1 = (Math.PI / 180) * start.latitude;
        double lat2 = (Math.PI / 180) * end.latitude;

        double lon1 = (Math.PI / 180) * start.longitude;
        double lon2 = (Math.PI / 180) * end.longitude;

        //地球半径
        double R = 6371;

        //两点间距离 km，如果想要米的话，结果*1000就可以了
        double d = Math.acos(Math.sin(lat1) * Math.sin(lat2) + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon2 - lon1)) * R;

        return d * 1000;
    }

    /**
     * Check whether the user is moving
     */
    public boolean isMoving() {
        //todo

        MovementDetector.getInstance().init(context);

        MovementDetector.getInstance().addListener(new MovementDetector.Listener() {
            @Override
            public void onMotionDetected(SensorEvent event, float acceleration) {
                isMoving = true;
            }
        });


        MovementDetector.getInstance().start();

        return isMoving;
    }

    /**
     * Receiver for intents sent by DetectedActivitiesIntentService via a sendBroadcast().
     * Receives a list of one or more DetectedActivity objects associated with the current state of
     * the device.
     */
    public class ActivityDetectionBroadcastReceiver extends BroadcastReceiver {
        protected static final String TAG = "activity-detection-response-receiver";

        @Override
        public void onReceive(Context context, Intent intent) {
            // received the intent sent from DetectionIntentService;

        }
    }

    /**
     * Everytime you need to stop the movement checking if you stop the application
     */
    public void stopMovementDetector() {
        MovementDetector.getInstance().stop();
    }
}
