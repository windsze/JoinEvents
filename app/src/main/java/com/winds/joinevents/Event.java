package com.winds.joinevents;

import org.joda.time.DateTime;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;

/**
 * Created by WindS on 18/9/2015.
 */
public class Event implements Serializable {
    // identifier
    private int eventType;
    private String eventTypeName;
    private int eventId;

    // event card page
    private String name;
    private int like_num;
    private int dislike_num;
    private int comment_num;
    private String cover_image;
    private String post_time;
    private String post_date;
    private String location;
    private int NumOfImages;

    // event details page
    private String from_time;
    private String from_date;
    private String to_time;
    private String to_date;
    private String contactNo; // xxx-xxxxxxxx
    private String contactPerson;
    private String content;
    private boolean isSponsored;
    private String price;
    private ArrayList<String> images = new ArrayList<>();
    private String lat;
    private String lng;

    // author
    private String author;
    private String author_id; // user_id
    private String profile_pic;

    //flag
    private boolean mAlert = false; // false by default

    // bookmark time
    private String bookmark_time;



    //=====================
    // getter & setter
    //=====================


    public String getEventTypeName() {
        return eventTypeName;
    }

    public void setEventTypeName(String eventTypeName) {
        this.eventTypeName = eventTypeName;
    }

    public String getBookmark_time() {
        return bookmark_time;
    }

    public void setBookmark_time(String bookmark_time) {
        this.bookmark_time = bookmark_time;
    }

    public int getNumOfImages() {
        return NumOfImages;
    }

    public void setNumOfImages(int numOfImages) {
        NumOfImages = numOfImages;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getAuthor_id() {
        return author_id;
    }

    public void setAuthor_id(String author_id) {
        this.author_id = author_id;
    }

    public boolean ismAlert() {
        return mAlert;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public String getCover_image() {
        return cover_image;
    }

    public void setCover_image(String cover_image) {
        this.cover_image = cover_image;
    }


    public void addImage(String imageLink) {
        images.add(imageLink);
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setmAlert(boolean mAlert) {
        this.mAlert = mAlert;
    }

    public boolean getmAlert() {
        return mAlert;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPost_time() {
        return post_time;
    }

    public void setPost_time(String post_time) {
        this.post_time = post_time;
    }

    public String getPost_date() {
        return post_date;
    }

    public void setPost_date(String post_date) {
        this.post_date = post_date;
    }

    public int getEventType() {
        return eventType;
    }

    public void setEventType(int eventType) {
        this.eventType = eventType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getFrom_time() {
        return from_time;
    }

    public void setFrom_time(String from_time) {
        this.from_time = from_time;
    }

    public String getFrom_date() {
        return from_date;
    }

    public void setFrom_date(String from_date) {
        this.from_date = from_date;
    }

    public String getTo_time() {
        return to_time;
    }

    public void setTo_time(String to_time) {
        this.to_time = to_time;
    }

    public String getTo_date() {
        return to_date;
    }

    public void setTo_date(String to_date) {
        this.to_date = to_date;
    }

    public int getLike_num() {
        return like_num;
    }

    public void setLike_num(int like_num) {
        this.like_num = like_num;
    }

    public int getDislike_num() {
        return dislike_num;
    }

    public void setDislike_num(int dislike_num) {
        this.dislike_num = dislike_num;
    }

    public int getComment_num() {
        return comment_num;
    }

    public void setComment_num(int comment_num) {
        this.comment_num = comment_num;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isSponsored() {
        return isSponsored;
    }

    public void setIsSponsored(boolean isSponsored) {
        this.isSponsored = isSponsored;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
