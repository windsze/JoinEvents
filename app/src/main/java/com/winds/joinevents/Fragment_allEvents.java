package com.winds.joinevents;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import mehdi.sakout.dynamicbox.DynamicBox;


public class Fragment_allEvents extends Fragment {
    // WHAT
    private static final String TAG = "front_page_fragment";
    private static final int FIRST_GET_EVENT_FINISH = 1;
    private static final int ON_LOAD_MORE_FINISH = 0;
    private static final int MORE_GET_EVENT_FINISH = 2;
    private static final int ADD_MY_FAVORITE_SUCCESS = 3;
    private static final int ADD_MY_FAVORITE_FAIL = 4;

    // Main view
    View v;


    private ArrayList<Event> myDatasetArr;
    private ArrayList<Event> tempNewDatasetArr;
    //public static final String ARG_PAGE = "ARG_PAGE";
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private LinearLayoutManager mLayoutManager;
//    private SwipeRefreshLayout mSwipelayout;
    private ProgressBar mProgressBar;
    private int mPage = 1;

    // Class
    private EventHandler eventHandler;
    private CardView cardView;


    // FLAG
    private static final int CARD_RECYCLER_VIEW = 1;
    private static boolean NO_MORE_EVENT_FOUND = false;


    //VARIABLE
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case ON_LOAD_MORE_FINISH:
                    // remove loading bar
                    removeLoadingRecyclerFooter();

                    if (NO_MORE_EVENT_FOUND == false) {
                        // add new items into list
                        myDatasetArr.addAll(tempNewDatasetArr);
                        mAdapter.notifyDataSetChanged(); // update all item with animation
                    }

                    // set key to false to open the door for next loading
                    isloading = false;
                    break;

                case FIRST_GET_EVENT_FINISH:
                    // first stop loading layout
                    box.hideAll();

                    if (myDatasetArr.size() == 0) {
                        Log.v(TAG, "empty dataset");
//                         empty dataset : show empty layout
                        View customView = getActivity().getLayoutInflater().inflate(R.layout.empty_record_layout, null, false);
                        box.addCustomView(customView, "empty");
                        box.showCustomView("empty");
                    }

                    setupAdapter();


                    break;
                case MORE_GET_EVENT_FINISH:

                    // update adapter
                    mAdapter.notifyDataSetChanged();
                    Toast.makeText(getActivity(), "Reload Finished", Toast.LENGTH_SHORT).show();

                    break;

                case ADD_MY_FAVORITE_SUCCESS:
                    Toast.makeText(getActivity(), "已加到我的最愛！", Toast.LENGTH_SHORT).show();

                    break;
                case ADD_MY_FAVORITE_FAIL:
                    Toast.makeText(getActivity(), "發生錯誤 請稍後再試", Toast.LENGTH_SHORT).show();

                    break;

            }
        }
    };


    private boolean isloading = false;

    // image loader
    DisplayImageOptions options;

    // dynamic box
    DynamicBox box;

    // share preference
    SharedPreferenceManager preference;

    //=================================
    // start
    //=================================
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_events, container, false);


        // INIT UI
//        mProgressBar = (ProgressBar) v.findViewById(R.id.progressBar);
//        mProgressBar.setVisibility(View.INVISIBLE);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
//
//        mSwipelayout = (SwipeRefreshLayout) v.findViewById(R.id.swiperefresh);
//        mSwipelayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
////                //reload dataset and update adapter
////                Log.w(TAG, "on swipe refreshing...");
////
////                new Thread(new Runnable() {
////                    @Override
////                    public void run() {
////                        try {
////                            myDatasetArr = eventHandler.get10EventsList_card(mPage);
////                        } catch (IOException e) {
////                            e.printStackTrace();
////                        }
////
////                        mHandler.sendEmptyMessage(MORE_GET_EVENT_FINISH);
////                    }
////                }).start();
//
//
//                //perform nothing after 1 second
//                try {
//                    Thread.sleep(1000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//
//                mSwipelayout.setRefreshing(false);
//            }
//        });

        mRecyclerView = (RecyclerView) v.findViewById(R.id.my_recycler_view);


        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(mLayoutManager) {

            @SuppressLint("LongLogTag")
            @Override
            public void onLoadMore(int current_page) {
                //display loading animation
                Log.v(TAG, "add null into dataset");


                if (!isloading) {
                    isloading = true;

                    Log.w(TAG, "isloading = true");

                    addLoadingRecyclerFooter();

                    // add more events into dataset
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            tempNewDatasetArr = new ArrayList<Event>();
                            //step2 - load 5 more data each time and add them to tail
//                        myDatasetArr.addAll(eventHandler.loadMoreEvents(5));
                            mPage++;
                            try {
                                ArrayList<Event> tempEventList = eventHandler.get10EventsList_card(mPage);
                                if (tempEventList.size() == 0) {
                                    // no more record found
                                    NO_MORE_EVENT_FOUND = true;
                                } else
                                    tempNewDatasetArr.addAll(tempEventList);
                            } catch (IOException e) {
                                e.printStackTrace();
                                box.showExceptionLayout();
                            }

                            mHandler.sendEmptyMessageDelayed(ON_LOAD_MORE_FINISH, 2000);
                        }
                    }).start();
                }

            }
        });

//        // Data set
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    myDatasetArr = eventHandler.get10EventsList_card(mPage);
                } catch (IOException e) {
                    e.printStackTrace();
                    box.showExceptionLayout();
                }

                // finish
                mHandler.sendEmptyMessage(FIRST_GET_EVENT_FINISH);
            }
        }).start();


        return v;
    }

    private void setupAdapter() {

        // make random order of data
        long seed = System.nanoTime();
        Collections.shuffle(myDatasetArr, new Random(seed));

        //
        mAdapter = new MyRecyclerAdapter(myDatasetArr, CARD_RECYCLER_VIEW) {

            /**
             * Get back layout components from adapter and setup onclick listener
             * @param holder Contains all layout components
             * @param position Certain position of layout. Can specify attribute for a certain position's item in layout.
             */
            @Override
            public void dynamicChangeOnLayout(RecyclerView.ViewHolder holder, final int position) {
//                Log.v(TAG, " In dynamicChangeOnLayout");
                if (holder != null) {
                    CardViewHolder lvh = (CardViewHolder) holder;


                    // main card
                    CardView mCard = lvh.mCard;

                    // card on click
                    mCard.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    // add browse record
                                    try {
                                        eventHandler.addBrowseHistory(myDatasetArr.get(position).getEventId(),preference.getEmail());
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }).start();

                            Bundle bundle = new Bundle();
                            bundle.putSerializable("event", myDatasetArr.get(position));
                            startActivity(new Intent(getActivity(), DetailEventActivity.class).putExtra("event", bundle));
                        }
                    });

                    // card components
                    TextView mTitle = lvh.mTitle;
                    TextView mAuthor = lvh.mAuthor;
                    TextView mAddress = lvh.mLocation;
                    TextView mLikeNum = lvh.mLikeNum;
                    TextView mDislikeNum = lvh.mDislikeNum;
                    TextView mCommentNum = lvh.mCommentNum;
                    TextView mPrice = lvh.mPrice;
                    TextView mPostDateTime = lvh.mPostDateTime;
                    ImageView mCoverImage = lvh.mCoverImage;
                    TextView mPhotoNum = lvh.mPhotoNum;

                    // card bottom
                    ImageButton mShare = lvh.mShare;
                    ImageButton mFavorite = lvh.mFavorite;

                    mTitle.setText(myDatasetArr.get(position).getName());
                    mAuthor.setText(myDatasetArr.get(position).getAuthor());
                    mAddress.setText(myDatasetArr.get(position).getLocation());
                    mLikeNum.setText(String.valueOf(myDatasetArr.get(position).getLike_num()));
                    mDislikeNum.setText(String.valueOf(myDatasetArr.get(position).getDislike_num()));
                    mCommentNum.setText(String.valueOf(myDatasetArr.get(position).getComment_num()));

                    if (String.valueOf(myDatasetArr.get(position).getPrice()).equals("0"))
                        mPrice.setText("免費");
                    else
                        mPrice.setText("$" + String.valueOf(myDatasetArr.get(position).getPrice()));
                    mPostDateTime.setText(myDatasetArr.get(position).getPost_date());

                    // number of image
                    if (myDatasetArr.get(position).getNumOfImages() > 1)
                        mPhotoNum.setText(myDatasetArr.get(position).getNumOfImages() + " photos");
                    else if (myDatasetArr.get(position).getNumOfImages() == 1)
                        mPhotoNum.setText(myDatasetArr.get(position).getNumOfImages() + " photo");
                    else mPhotoNum.setText("No Photo");


                    // Load image, decode it to Bitmap and display Bitmap in ImageView (or any other view
                    //  which implements ImageAware interface)
                    Log.v(TAG, "now display image by ImageLoader");

                    // less useful security checking as cover image must be existed
                    if (!myDatasetArr.get(position).getCover_image().equals("null"))
                        ImageLoader.getInstance().displayImage(myDatasetArr.get(position).getCover_image(), mCoverImage, options);

                    // card's more button on click
                    TextView mMore = lvh.mMore;
                    mMore.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
//                            Toast.makeText(getActivity(), "More OnClick", Toast.LENGTH_SHORT).show();
//                            Log.v(TAG, "More on position " + position + " on click");
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    // add browse record
                                    try {
                                        eventHandler.addBrowseHistory(myDatasetArr.get(position).getEventId(),preference.getEmail());
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }).start();

                            Bundle bundle = new Bundle();
                            bundle.putSerializable("event", myDatasetArr.get(position));
                            startActivity(new Intent(getActivity(), DetailEventActivity.class).putExtra("event", bundle));
                        }
                    });

                    // bookmark button onclick
                    mFavorite.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            new MaterialDialog.Builder(getActivity())
                                    .content("你確定要加到 我的最愛頁 嗎？")
                                    .positiveText("加入")
                                    .negativeText("不要")
                                    .callback(new MaterialDialog.ButtonCallback() {
                                        @Override
                                        public void onPositive(MaterialDialog dialog) {
                                            super.onPositive(dialog);

                                            addEventToMyFavorite(myDatasetArr.get(position).getEventId(), preference.getEmail()); // hardcode userid --> todo
                                        }
                                    })
                                    .build().show();

                        }
                    });

                } else
                    Log.v(TAG, "Holder is null");
            }
        };

        mRecyclerView.setAdapter(mAdapter);

    }

    private void addEventToMyFavorite(final int event_id, final String user_email) {
        // get eventid, add eventid to db myfavorite table with userid.

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (eventHandler.saveToMyFavoritePage(event_id, user_email))
                        mHandler.sendEmptyMessage(ADD_MY_FAVORITE_SUCCESS);
                    else
                        mHandler.sendEmptyMessage(ADD_MY_FAVORITE_FAIL);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();


    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // INIT COMPONENTS
        eventHandler = new EventHandler(getActivity());


        // Create global configuration and initialize ImageLoader with this config
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
                .build();

        ImageLoader.getInstance().init(config);

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.ic_action_reload)
                .showImageForEmptyUri(R.drawable.ic_crop_free)
                .showImageOnFail(R.drawable.ic_error_outline)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .displayer(new SimpleBitmapDisplayer())
                .build();

        preference = new SharedPreferenceManager(getActivity());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.v(TAG, "box using recyclerview");
        box = new DynamicBox(getActivity(), mRecyclerView);
        box.showLoadingLayout();
    }

    @Override
    public void onPause() {
        super.onPause();
    }


    private void addLoadingRecyclerFooter() {
        /**
         * in adapter we set bindview to progressBar layout once the dataset is detected to be null.
         * Therefore we add null value to the dataset array.
         */
        myDatasetArr.add(null);

        /**
         * Call notifyDataSetChanged to adapter for adding new row.
         */
        mAdapter.notifyDataSetChanged();
    }

    private void removeLoadingRecyclerFooter() {
        /**
         * remove the null data we added in addLoadingRecyclerFooter()
         * */
        myDatasetArr.remove(myDatasetArr.size() - 1);

        /**
         * need to notify adapter that dataset has changed to remove the progress bar
         */
        mAdapter.notifyItemRemoved(myDatasetArr.size() - 1);

    }


}
