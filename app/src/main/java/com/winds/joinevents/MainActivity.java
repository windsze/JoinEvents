package com.winds.joinevents;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.res.Configuration;
import android.location.Location;
import android.os.Build;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.appyvet.rangebar.IRangeBarFormatter;
import com.appyvet.rangebar.RangeBar;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {
    // activity_main
    CoordinatorLayout mCoordinatorLayout;
    DrawerLayout mDrawerLayout;
    TabLayout mTabLayout;
    NavigationView mNavigationView;
    Toolbar mToolbar;

    // nav_toggle
    private ActionBarDrawerToggle mDrawerToggle;

    SharedPreferenceManager sharedPreferenceManager;

    private final String TAG = "MainActivity";


    //test, del
    NetworkHandler mNetworkHandler;

    // filter


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        // Initialize the SDK before executing any other operations,
        // especially, if you're using Facebook UI elements.

        setContentView(R.layout.activity_main);

        // bind all the ui components
        initUI();

        // setup navigation toggle as button which will animate automatically in toolbar
        mDrawerToggle = new ActionBarDrawerToggle(MainActivity.this, mDrawerLayout, R.string.hello_world, R.string.hello_world);

        // setup toolbar as actionbar
        setSupportActionBar(mToolbar);
        getSupportActionBar().setElevation(1); //set divider line
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // setup all items' listener
        setupOnClickListener();

        // setup viewpager and tablayout
        initViewPagerAndTabs();

//        // start notification intent
        startDetectionService();

    }

    private void startDetectionService() {
        //
        if (sharedPreferenceManager == null)
            sharedPreferenceManager = new SharedPreferenceManager(this);

        startService(new Intent(MainActivity.this, LocationService.class));
        if (!sharedPreferenceManager.getServiceRunningStatus()) {
            Log.e(TAG, "start service...");

            // 创建所需要启动的Service的Intent
            startService(new Intent(MainActivity.this, LocationService.class));

        } else
            Log.e(TAG, "service is already exist and running");

    }

    PagerAdapter pagerAdapter;
    ViewPager viewPager;
    Fragment fragment_all = new Fragment_allEvents();

    private void initViewPagerAndTabs() {
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        pagerAdapter = new PagerAdapter(getSupportFragmentManager());

        pagerAdapter.addFragment(fragment_all, "所有活動");
//        pagerAdapter.addFragment(new Fragment(), "我的附近");
        pagerAdapter.addFragment(new Fragment_allSameTypeEvents(1), "休閒活動");
        pagerAdapter.addFragment(new Fragment_allSameTypeEvents(2), "飲飲食食");
        pagerAdapter.addFragment(new Fragment_allSameTypeEvents(3), "購物");

        //pagerAdapter.addFragment(Fragment_ViewPager.createInstance(4), "tab 2");
        viewPager.setAdapter(pagerAdapter);
        mTabLayout = (TabLayout) findViewById(R.id.tabLayout);
        mTabLayout.setupWithViewPager(viewPager);


    }


    private void initUI() {
        // VARIABLE
        mNetworkHandler = new NetworkHandler(this);

        // UI
        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.rootLayout);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mTabLayout = (TabLayout) findViewById(R.id.tabLayout);
        mNavigationView = (NavigationView) findViewById(R.id.navigationView);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);

    }


    private void setupOnClickListener() {
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.addEvents:
                        //show add event page
                        startActivity(new Intent(MainActivity.this, newEventActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
//
                        return true;
                    case R.id.myfavoriatePage:
                        startActivity(new Intent(MainActivity.this, MyFavoriteActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                        return true;
                }


                Snackbar snackbar = Snackbar.make(mNavigationView, "onClicked", Snackbar.LENGTH_LONG);
                snackbar.setAction("Dismiss", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
                snackbar.show();


                return true;
            }
        });

        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (sharedPreferenceManager == null)
            sharedPreferenceManager = new SharedPreferenceManager(this);

        if (!sharedPreferenceManager.isLoggedIn())
            getMenuInflater().inflate(R.menu.menu_main_not_signin, menu);
        else
            getMenuInflater().inflate(R.menu.menu_main_loggedin, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item))
            return true;
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.login:
                startActivity(new Intent(MainActivity.this, loginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                return true;


            case R.id.action_settings:
                startActivity(new Intent(MainActivity.this, SettingActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                return true;

            case R.id.logout:
                sharedPreferenceManager.logoutUser();
                return true;

            case R.id.map:
                startActivity(new Intent(MainActivity.this, MapTestingActivity.class));
                return true;

            case R.id.db:
                Intent dbmanager = new Intent(MainActivity.this, AndroidDatabaseManager.class);
                startActivity(dbmanager);
                return true;

//            case R.id.startSer:
////
//                startService(new Intent(MainActivity.this, LocationService.class));
////
//                return true;
            case R.id.filter:
                // display a dialog
                MaterialDialog filter_dialog = new MaterialDialog.Builder(MainActivity.this)
                        .title("Filter")
                        .customView(R.layout.filter_content, true)
                        .positiveText("Confirm")
                        .negativeText("Cancel")
                        .callback(new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onPositive(MaterialDialog dialog) {
                                super.onPositive(dialog);

                                // todo start another fragment
                                Fragement_filtered filterFragement = new Fragement_filtered();
                                Bundle bundle = new Bundle();
                                bundle.putInt("price_to", filter_price_to);
                                bundle.putInt("price_from", filter_price_from);

                                bundle.putString("location", filter_location);
                                bundle.putInt("type", filter_type);
                                filterFragement.setArguments(bundle);


//                                getFragmentManager().beginTransaction().replace(fragment_all, filterFragement).commit();
                                pagerAdapter.addFragment(filterFragement, "Filtering");
                                pagerAdapter.notifyDataSetChanged();
//                                FragmentTransaction trans = getFragmentManager().beginTransaction();
                                viewPager.setAdapter(pagerAdapter);
//                                pagerAdapter.notifyDataSetChanged();

                                mTabLayout.setupWithViewPager(viewPager);
                                Log.e(TAG, "pagerAdapter changed");
                            }

                            @Override
                            public void onNegative(MaterialDialog dialog) {
                                super.onNegative(dialog);
                            }
                        }).build();

                View view = filter_dialog.getCustomView();
                Spinner type_spinner = (Spinner) view.findViewById(R.id.type_spinner);
                Spinner location_spinner = (Spinner) view.findViewById(R.id.location_spinner);
                RangeBar price_rangeBar = (RangeBar) view.findViewById(R.id.rangebar);

                                /* define all items in spinners */

                // Create an ArrayAdapter using the string array and a default spinner layout
                ArrayAdapter<CharSequence> types_adapter = ArrayAdapter.createFromResource(getApplicationContext(),
                        R.array.types_array, android.R.layout.simple_spinner_item);
                // Specify the layout to use when the list of choices appears
                types_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                // Apply the adapter to the spinner
                type_spinner.setAdapter(types_adapter);


                ArrayAdapter<CharSequence> location_adapter = ArrayAdapter.createFromResource(getApplicationContext(),
                        R.array.location_array, android.R.layout.simple_spinner_item);
                // Specify the layout to use when the list of choices appears
                location_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                // Apply the adapter to the spinner
                location_spinner.setAdapter(location_adapter);


                                /* define price range bar */
                price_rangeBar.setFormatter(new IRangeBarFormatter() {
                    @Override
                    public String format(String s) {
                        // Transform the String s here then return s
                        return "HKD " + s;
                    }
                });

                                /* define handler */
                price_rangeBar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
                    @Override
                    public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex,
                                                      int rightPinIndex,
                                                      String leftPinValue, String rightPinValue) {
                        Log.e(TAG, "price_from:" + leftPinValue);
                        Log.e(TAG, "price_to:" + rightPinValue);
                        Log.e(TAG, "leftPinIndex:" + leftPinIndex);
                        Log.e(TAG, "rightPinIndex:" + rightPinIndex);
                        filter_price_from = Integer.parseInt(leftPinValue);
                        filter_price_to = Integer.parseInt(rightPinValue);
                    }
                });

                type_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        Log.e(TAG, "position:" + position);
                        Log.e(TAG, "id :" + id);

                        if (filter_type == 0)
                            filter_type = -1;
                        else
                            filter_type = position;
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                location_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        Log.e(TAG, "position:" + position);
                        Log.e(TAG, "id :" + id);

                        // change from position to location_String
                        if (position == 0)
                            filter_location = null;
                        else
                            filter_location = getResources().getStringArray(R.array.location_array)[position];
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });


                filter_dialog.show();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    //todo rearrange variable location
    int filter_type = -1;
    String filter_location = null;
    int filter_price_from = 0; // default
    int filter_price_to = 5000; // default

    @Override
    protected void onResume() {
        super.onResume();

        // Logs 'install' and 'app activate' App Events.
        //AppEventsLogger.activateApp(this);

        // CHECK LOGIN STATUS
        if (sharedPreferenceManager != null) {
            if (sharedPreferenceManager.isLoggedIn()) {
                // change navigation username
                TextView username = (TextView) mDrawerLayout.getRootView().findViewById(R.id.nav_username);
                username.setText(sharedPreferenceManager.getUsername());

                TextView email = (TextView) mDrawerLayout.getRootView().findViewById(R.id.nav_email);
                email.setText(sharedPreferenceManager.getEmail());

                // change navigation login type icon
                int loginType = sharedPreferenceManager.getLoginType();
                if (loginType == 1) {
                    // join event
                    ImageView loginTypeImage = (ImageView) mDrawerLayout.getRootView().findViewById(R.id.imageView_loginType);
                    loginTypeImage.setImageResource(R.mipmap.ic_launcher);

                } else if (loginType == 0) {
                    // fb
                    ImageView loginTypeImage = (ImageView) mDrawerLayout.getRootView().findViewById(R.id.imageView_loginType);
                    loginTypeImage.setImageResource(R.mipmap.ic_facebook_log);
                } else
                    // loginType == 0 <-- error
                    Log.v(TAG, "error in loginType : -1");
            }
        }
    }


    @Override
    protected void onPause() {
        super.onPause();

        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }

    static class PagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmentTitleList = new ArrayList<>();

        public PagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public void addFragment(Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
        }

        @Override
        public int getItemPosition(Object object) {
            return PagerAdapter.POSITION_NONE;
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitleList.get(position);
        }
    }


}
