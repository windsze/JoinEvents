package com.winds.joinevents;


import android.app.Activity;
import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.List;

import br.com.goncalves.pugnotification.notification.PugNotification;


public class LocationService extends Service implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    // flag
    private static final String TAG = "Test_LocationService";
    private static final int TIME_IS_UP = 0;
    private boolean RUN_THREAD = true;
    private static final int CLOSE_DISTANCE = 1000; // 1000 meters

    // delay time
    private static final int TIME_INTERVAL = 60 * 30 * 8; // 4 hours
    private static final int TO_MILLISEC = 1000;

    // class
    NotificationHandler notificationHandler;
    Location resultLocation;
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    LocationQueryHandler locationServiceHandler;
    SharedPreferenceManager preferenceManager;


    // object
    Thread thread;
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case TIME_IS_UP:
                    Log.e(TAG, "reach message");
                    if (mGoogleApiClient.isConnected())
                        updateLocation();
                    break;
            }
        }
    };


    /**
     * Service lifecycle: onCreate -> onStartCommand -> running -> onDestroy
     * Therefore, onCreate is called for initialing all object and class, and connect google play service
     */
    @Override
    public void onCreate() {
        Log.e(TAG, "onCreate");

        // init
        initComponents();

        // Create an instance of GoogleAPIClient
        connectGooglePlay();

        // setup location update request
        createLocationRequest();

        // infinite background checking
        preferenceManager.setServiceRunningStatus(true);

        super.onCreate();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand");

        new Thread(new Runnable() {
            @Override
            public void run() {
//
                while (true) {
                    try {
                        Thread.sleep(TO_MILLISEC * TIME_INTERVAL);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    if (mGoogleApiClient.isConnected()) {
                        updateLocation();
                    } else {
                        connectGooglePlay();
                    }
                }

            }
        });

//        handler.post(workLoop);

        return super.onStartCommand(intent, flags, startId);
    }

//
//    private Runnable workLoop = new Runnable() {
//        @Override
//        public void run() {
//            Log.e(TAG, "now open new thread...");
//            new Thread() {
//                @Override
//                public void run() {
//                    Log.e(TAG, "send empty message...");
//                    handler.sendEmptyMessage(TIME_IS_UP);
//                    // at the end, post on delay
//                    handler.postDelayed(this, TIME_INTERVAL * TO_MILLISEC);
//                    Log.e(TAG, "wait for next...");
//                }
//            }.start();
//        }
//    };


    private void updateLocation() {
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
    }

    private void stopUpdateLocation() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
    }


    @Override
    public void onLocationChanged(Location location) {
        Log.e(TAG, "receive location changed");
        this.resultLocation = location;
        Log.e(TAG, "new location is:" + resultLocation.getLatitude() + "," + resultLocation.getLongitude());

        String noti_title = resultLocation.getLatitude() + "," + resultLocation.getLongitude();
        String noti_msg = locationServiceHandler.getOneAddress(new LatLng(resultLocation.getLatitude(), resultLocation.getLongitude()));
        // writeLog for location details
        try {
            writeLog(noti_title, noti_msg);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // check events
        checkCloseEvents();

        // stop update
        stopUpdateLocation();

    }


    /**
     * Write the log details to db
     *
     * @param noti_title location geocode
     * @param noti_msg   address regards to the geocode
     * @return success or not
     * @throws IOException
     */
    private void writeLog(final String noti_title, final String noti_msg) throws IOException {

        new Thread(new Runnable() {
            @Override
            public void run() {
                Request request = new Request.Builder()
                        .url("http://42.2.152.193:8080/log/post_newLog.php?" +
                                        "user_id=" + "1" + "&" +
                                        "content=" + noti_title + noti_msg
                        )
                        .build();
                OkHttpClient okHttpClient = new OkHttpClient();
                Response response = null;

                try {
                    response = okHttpClient.newCall(request).execute();
                    String responseStr = response.body().string();
                    Log.v(TAG, "response is " + responseStr);

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }).start();
    }

    /**
     * This method checks the sqlite whether there is close events nearby user
     * If yes, it sends notification to user
     *
     * @return is having event or not
     */
    private boolean checkCloseEvents() {
        notificationHandler = new NotificationHandler(this);
        List<Event> events = notificationHandler.searchCloseEvent(new LatLng(resultLocation.getLatitude(), resultLocation.getLongitude()), CLOSE_DISTANCE);
        String msg = "";

        if (events.size() > 0) {
            StringBuilder msgBuilder = new StringBuilder();

            for (int i = 0; i < events.size(); i++) {
                msgBuilder.append(events.get(i).getName() + "\n");
            }

            msg = msgBuilder.toString();
        }

        if (msg.length() > 0) {
            makeNotification("你有鄰近活動！", msg);
            return true;
        }

        return false;
    }

    /**
     * When googleApiClient is connected successfully
     * onConnected will be called automatically
     */
    @Override
    public void onConnected(Bundle bundle) {
        // call when mGoogleApiClient is connected
        Log.e(TAG, "mGoogleApiClient is connected");

        updateLocation();
    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(TAG, "onConnectionFailed:" + connectionResult);
    }


    /**
     * Construct the notification
     *
     * @param title Notification title
     * @param msg   Notification message
     */
    private void makeNotification(String title, String msg) {
        PugNotification.with(this)
                .load()
                .title(title)
                .message(msg)
                .smallIcon(R.mipmap.ic_launcher)
                .largeIcon(R.mipmap.ic_launcher) //
                .flags(Notification.DEFAULT_VIBRATE)
                .simple()
                .build();
    }

    /**
     * Bind the service for application.
     * If binded, When application is killed, service dies.
     *
     * @param intent
     * @return
     */
    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }


    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(0);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * When service is dead, onDestroy will be called.
     * Here we release the resources by
     * i) disconnect the GoogleApiClient
     * ii) stop the thread
     * iii) set the service running status in sharepreference to false
     */
    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");

        // 1. end connection with google api
        Log.e(TAG, "mGoogleApiClient.disconnected()");
        mGoogleApiClient.disconnect();

        // 2. end infinite loop
        RUN_THREAD = false;

        // 3. end thread
        thread.interrupt();
        thread = null;

        // 4. end service
        preferenceManager.setServiceRunningStatus(false);
        Log.e(TAG, "service running status is false");

        super.onDestroy();
    }

    private void initComponents() {
        locationServiceHandler = new LocationQueryHandler(this);
        preferenceManager = new SharedPreferenceManager(this);


        // set default service running state to false
        preferenceManager.setServiceRunningStatus(false);

    }

    /**
     * Create the instance of googleApiClient
     * and connect to API meanwhile
     */
    private void connectGooglePlay() {
        Log.e(TAG, "connectGooglePlay()");
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        // equals to onStart()
        mGoogleApiClient.connect();

    }

}
