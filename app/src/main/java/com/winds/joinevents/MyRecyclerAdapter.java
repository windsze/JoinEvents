package com.winds.joinevents;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

import it.gmariotti.cardslib.library.recyclerview.internal.BaseRecyclerViewAdapter;

public abstract class MyRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {
    // FLAG
    private static final int LIST_RECYCLER_VIEW = 0;
    private static final int CARD_RECYCLER_VIEW = 1;
    private static final int DETAIL_RECOMMEND_VIEW = 2;
    private static final int PROGRESS_BAR_VIEW = 3;

    private static final String TAG = "MyRecyclerAdapter";

    // VIEW TYPE
    private int viewType = 0;

    //DATA
    private ArrayList<Event> mDataSet;

    //======================================================

    /**
     * These static classes are used to define our layout components in our view
     * indispensable
     */
    //======================================================

    public abstract class MyFavoriteListViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "MyFavoriteListViewHolder";
        // each data item is just a string in this case
        TextView mTitle, mAuthor, mAddBookmarkDateTime, mType, mLocation;
        ImageView mCover;
        RelativeLayout mItemView;
//        ImageButton mAlarmBtn_ImageBtn;
        CheckBox mAlertCheck;


        public MyFavoriteListViewHolder(View v) {
            super(v);

//            Log.v(TAG, "in MyFavoriteListViewHolder");
            mItemView = (RelativeLayout) v.findViewById(R.id.mItemView);
            mTitle = (TextView) v.findViewById(R.id.myfavorite_mTitle);
            mAuthor = (TextView) v.findViewById(R.id.mAuthor_myfavorite);
            mAddBookmarkDateTime = (TextView) v.findViewById(R.id.myfavorite_addDate);
            mType = (TextView) v.findViewById(R.id.mHashtag_myfavorite);
            mLocation = (TextView) v.findViewById(R.id.mLocation_myfavorite);

            mCover = (ImageView) v.findViewById(R.id.mCoverPhoto_myFavorite);
            mAlertCheck = (CheckBox) v.findViewById(R.id.mAlertCheck);
//            mAlarmBtn_ImageBtn = (ImageButton) v.findViewById(R.id.mAlarm_btn_imageBtn);

        }

    }

    /**
     * first page of events list
     */
    public static abstract class CardViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "CardViewHolder";
        // each data item is just a string in this case
        TextView mTitle, mAuthor, mHashtag, mLocation, mLikeNum,
                mDislikeNum, mCommentNum, mPrice, mPostDateTime, mPhotoNum;
        ImageButton mShare, mFavorite;
        Button mMore;
        CardView mCard;
        ImageView mCoverImage;

        // position
        int position;

        public CardViewHolder(View v) {
            super(v);

//            Log.v(TAG, "in CardViewHolder");
            mCard = (CardView) v.findViewById(R.id.event_card);
            mTitle = (TextView) v.findViewById(R.id.event_title);
            mAuthor = (TextView) v.findViewById(R.id.event_author);
            mHashtag = (TextView) v.findViewById(R.id.event_hashtag1);
            // forget the hashtag 2 first
            mLocation = (TextView) v.findViewById(R.id.event_location);
            mLikeNum = (TextView) v.findViewById(R.id.event_like_num);
            mDislikeNum = (TextView) v.findViewById(R.id.event_dislike_num);
            mCommentNum = (TextView) v.findViewById(R.id.event_comment_num);
            mPrice = (TextView) v.findViewById(R.id.event_price);
            mPostDateTime = (TextView) v.findViewById(R.id.event_time);

            // button
            mShare = (ImageButton) v.findViewById(R.id.mBtn_share);
            mFavorite = (ImageButton) v.findViewById(R.id.mBtn_bookmark);
            mMore = (Button) v.findViewById(R.id.event_more_button);

            //image
            mCoverImage = (ImageView) v.findViewById(R.id.event_cover_image);

            // number of image
            mPhotoNum = (TextView) v.findViewById(R.id.event_photo_num);


        }
    }


    /**
     * Recommend list in detailed page
     */


    public static abstract class DetailRecommendViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "DetailRecommendViewHolder";
        // each data item is just a string in this case
        TextView mTitle, mAuthor, mLocation, mPrice, mPhotoNum, mPostDateTime;
        ImageView mCover;
        CardView mCard;

        public DetailRecommendViewHolder(View v) {
            super(v);

//            Log.v(TAG, "in CardViewHolder");

            mCard = (CardView) v.findViewById(R.id.event_card);
            mTitle = (TextView) v.findViewById(R.id.event_title);
//            mAuthor = (TextView) v.findViewById(R.id.event_author);
//            mHashtag = (TextView) v.findViewById(R.id.event_hashtag1);
            mLocation = (TextView) v.findViewById(R.id.event_location);
            mPostDateTime = (TextView) v.findViewById(R.id.event_time);
            mPrice = (TextView) v.findViewById(R.id.event_price);
            mPhotoNum = (TextView) v.findViewById(R.id.event_photo_num);
            mCover = (ImageView) v.findViewById(R.id.event_cover);

        }
    }

    /**
     * For displaying progress bar as one of the item in list
     */
    public static abstract class ProgressbarViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "CardViewHolder";
        public ProgressBar progressBar;

        public ProgressbarViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        }

    }

    //======================================================

    /**
     * Those method define the layout inflate dynamically
     */
    //======================================================
    public MyRecyclerAdapter(ArrayList<Event> mDataSet, int viewType) {
        this.mDataSet = mDataSet;
        this.viewType = viewType;
    }

    @Override
    public int getItemViewType(int position) {

        if (mDataSet.get(position) != null) {
//            Log.w(TAG,"mDataSet.get("+position+") != null");
            return this.viewType;
        } else if (mDataSet.get(position) == null) {
            // is progress item
//            Log.w(TAG,"mDataSet.get("+position+") == null");
            return PROGRESS_BAR_VIEW;
        }
        return -1;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
//        Log.v(TAG,"InOnCreateViewHolder, viewType is: "+viewType);

        if (viewType == LIST_RECYCLER_VIEW) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_layout_myfavorite, parent, false);
            return new MyFavoriteListViewHolder(v) {};

        } else if (viewType == CARD_RECYCLER_VIEW) {
//            Log.w(TAG,"viewType == card_recycler_view");
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_card, parent, false);
            return new CardViewHolder(v) {};

        } else if (viewType == DETAIL_RECOMMEND_VIEW) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_card_single, parent, false);
            return new DetailRecommendViewHolder(v) {};

        } else if (viewType == PROGRESS_BAR_VIEW) {
//            Log.w(TAG,"viewType == progress_bar_view");
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.progress_bar, parent, false);
            return new ProgressbarViewHolder(v) {};
        }

        return null;
    }

    public abstract void dynamicChangeOnLayout(RecyclerView.ViewHolder holder, int position);

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        // apply all holder's components inside the view in certain position

//        Log.v(TAG, "in onBindViewHolder position: " + position);

        if (holder instanceof MyFavoriteListViewHolder) {
//            Log.v(TAG,"holder instanceof MyFavoriteListViewHolder");
            MyFavoriteListViewHolder lvh = (MyFavoriteListViewHolder) holder;

            dynamicChangeOnLayout(lvh, position);
//            performCardOnClick(position);

        } else if (holder instanceof CardViewHolder) {
//            Log.v(TAG, "holder instanceof CardViewHolder");
            CardViewHolder cvh = (CardViewHolder) holder;

            dynamicChangeOnLayout(cvh, position);
//            performCardOnClick(position);

        } else if (holder instanceof DetailRecommendViewHolder) {
            DetailRecommendViewHolder drv = (DetailRecommendViewHolder) holder;

            dynamicChangeOnLayout(drv, position);

        } else if (holder instanceof ProgressbarViewHolder) {
            ((ProgressbarViewHolder) holder).progressBar.setIndeterminate(true);
        }

    }


    @Override
    public int getItemCount() {
        return mDataSet.size();
    }



}