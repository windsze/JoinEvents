package com.winds.joinevents;

import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

public class registerActivity extends AppCompatActivity {
    // UI
    Toolbar mToolbar;
    TextInputLayout mEmail_input, mPassword_input, mLastname_input, mFirstname_input, mContactPerson_input, mPhone_input;
    EditText mConfirmPassword;
    RadioButton mMale, mFemale;
    Button mSubmit;
    TextView mPasswordHelper, mDob;

    // VARIABLE


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        initUIComponents();

        setupToolbar();

        setupOnClick();
    }

    private void setupToolbar(){
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mToolbar.setTitle(R.string.toolbar_title);

    }
    private void initUIComponents(){
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mEmail_input = (TextInputLayout) findViewById(R.id.register_email_inputLayout);
        mPassword_input = (TextInputLayout) findViewById(R.id.register_password_inputLayout);
        mLastname_input = (TextInputLayout) findViewById(R.id.lastName_inputLayout);
        mFirstname_input = (TextInputLayout) findViewById(R.id.firstName_inputlayout);
        mContactPerson_input = (TextInputLayout) findViewById(R.id.registerContact_name_input);
        mPhone_input = (TextInputLayout) findViewById(R.id.registerContact_phone_input);

        mConfirmPassword = (EditText) findViewById(R.id.register_confirmPassword);

        mMale = (RadioButton) findViewById(R.id.radioButton_male);
        mFemale = (RadioButton) findViewById(R.id.radioButton_female);

        mSubmit = (Button) findViewById(R.id.mBtn_create);

        mDob = (TextView) findViewById(R.id.register_birthday);
        mPasswordHelper = (TextView) findViewById(R.id.register_password_helper);

    }
    private void setupOnClick(){
        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private boolean isAllInfoPrepared(){
        // check any empty edittext

        // email
        if(mEmail_input.getEditText().length()==0){
            mEmail_input.setError("Cannot be empty");

        }else{
            mEmail_input.setErrorEnabled(false);
            if (!mEmail_input.getEditText().toString().matches("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\\\.[A-Z]{2,6}$")){
                mEmail_input.setError("Wrong email");
            }else
                mEmail_input.setErrorEnabled(false);
        }

        // password and confirm
        if (mPassword_input.getEditText().length()==0){
            // password empty
            mPasswordHelper.setTextColor(getResources().getColor(R.color.bpRed));
        }else{
            if (mConfirmPassword.getText().length()>0){
                if (!mConfirmPassword.getText().toString().equals(mPassword_input.getEditText().toString())){
                    //password does not match
                    mConfirmPassword.setError("Password does not match");
                }
            }else{
                // confirmpassword is empty
                mConfirmPassword.setError("Cannot be empty");
            }
        }

        // name
        if (mFirstname_input.getEditText().length()==0){
            mFirstname_input.setError("Cannot be empty");
        }else
            mFirstname_input.setErrorEnabled(false);

        if (mLastname_input.getEditText().length()==0){
            mLastname_input.setError("Cannot be empty");
        }else
            mLastname_input.setErrorEnabled(false);

        // other information can be ignored


        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
