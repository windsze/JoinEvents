package com.winds.joinevents;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.preference.PreferenceManager;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import java.util.logging.LogManager;

/**
 * Created by WindS on 30/9/2015.
 */
public class SharedPreferenceManager {
    private SharedPreferences prefs;
    SharedPreferences.Editor editor;
    Context _context;

    int PRIVATE_MODE  = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "login_session";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_PASSWORD = "password";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";

    // TAG
    private final String TAG = "SharedPreferenceManager";


    // =========================
    // Methods
    // =========================
    public SharedPreferenceManager(Context context) {
        _context = context;
        prefs = PreferenceManager.getDefaultSharedPreferences(_context);
        editor = prefs.edit();
    }
    /**
     * Quick check and get data for login
     * **/
    public boolean isLoggedIn(){
        return prefs.getBoolean(IS_LOGIN, false);
    }

    /**
     * Create login session
     * */
    public void createLoginSession(){
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        // commit changes
        editor.commit();
    }

    public String getUsername(){
        return prefs.getString("username","");
    }
    public int getLoginType(){
        return prefs.getInt("loginType",0);
    }
    public String getEmail(){ return prefs.getString("email",""); }
    /*
    * When user uses different ways to login,
    * updates the loginType instantly
    * for navigation bar display correct icon
    * */
    public void changeLoginType(int type){
        // 0 = facebook
        // 1 = joinevent
        if (type == 1){
            editor.putInt("loginType",1);
        }else
            editor.putInt("loginType",0);
        editor.commit();
    }
    /**
     * Clear session details
     * */
    public void logoutUser(){

        if (AccessToken.getCurrentAccessToken() != null){
            // logout from facebook
            LoginManager.getInstance().logOut();

            Log.v(TAG, "logout user from facebook btn OK! ");
        }

        if (AccessToken.getCurrentAccessToken() == null){

            // already logged out
            Log.v(TAG, "loginManager.getInstance is null");

            // Clearing all data from Shared Preferences
            editor.clear();
            editor.commit();

            Log.v(TAG, "clear user's info from editor success");
        }else
            Log.v(TAG, "havent successfully logged in!! be careful");



        // After logout redirect user to Loing Activity
        Intent i = new Intent(_context, MainActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        _context.startActivity(i);

    }

    public void storeUserDetails(User user){

        /*
        * user.setEmail(obj.getString("emailAddress"));
            user.setUsername(obj.getString("accountName"));
            user.setLoginType(obj.getInt("loginType"));
            user.setRegistrationDate(obj.getString("registrationDate"));
            user.setLast_login_date(obj.getString("last_login_date"));
            user.setGender(obj.getString("sex"));
            user.setDob(obj.getString("dob"));
            user.setUserType(obj.getInt("userType"));
            user.setPoint(obj.getInt("points"));
            */
        editor.putString("email",user.getEmail());
        editor.putString("username",user.getUsername());
        editor.putString("registrationDate",user.getRegistrationDate());
        editor.putString("last_login_date",user.getLast_login_date());
        editor.putInt("loginType",user.getLoginType());
        editor.putInt("point",user.getPoint());
        editor.putString("gender",user.getGender());
        editor.putString("dob",user.getDob());
        editor.putInt("userType",user.getUserType());

        editor.commit();
    }

    public void setServiceRunningStatus(boolean status){
        editor.putBoolean("isServiceRunning", status);
        editor.commit();
    }

    public boolean getServiceRunningStatus(){
        return prefs.getBoolean("isServiceRunning", false);
    }

    /**
     * Put ObJect any type into SharedPrefrences with 'key' and save
     * @param key SharedPreferences key
     * @param obj is the Object you want to put
     */
    private void putObject(String key, Object obj){
        Gson gson = new Gson();
        editor.putString(key, gson.toJson(obj));
        editor.commit();
    }


    public void setCurrentLocation(Location resultLocation) {
        editor.putString("currentLocation",""+resultLocation.getLatitude()+","+resultLocation.getLongitude());
        editor.commit();
    }
    public LatLng getCurrentLocation(){
        String locationStr = prefs.getString("currentLocation","");

        if (locationStr.length() > 0){
            Double lat = Double.valueOf(locationStr.split(",")[0]);
            Double lng = Double.valueOf(locationStr.split(",")[1]);

            return new LatLng(lat,lng);
        }

        return null;
    }
}
