package com.winds.joinevents;

import android.util.Log;

import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;


public class JSONHandler {
    private static final String TAG = "JSONHandler";
    /*
        * Handler JSON response from server
        * 1/ convert response from JSON to Java
        * */
    private final OkHttpClient client = new OkHttpClient();
    private final Gson gson = new Gson();

    //=======
    // method
    //=======
    public ArrayList parseEventsList_card(String JSONStr) {
        ArrayList<Event> events = new ArrayList<>();

        if (JSONStr.contains("error") || JSONStr.length() == 0) {
            //will not be error
        } else {
            try {
                JSONArray events_json_array = new JSONArray(JSONStr);
                if (events_json_array.length() > 0) {
                    for (int i = 0; i < events_json_array.length(); i++) {
                        JSONObject jsonEvent = events_json_array.getJSONObject(i);
                        Event event = new Event();
                        event.setName(jsonEvent.getString("name"));
                        event.setEventType(jsonEvent.getInt("type"));
                        event.setEventId(jsonEvent.getInt("id"));
                        event.setAuthor(jsonEvent.getString("author_name"));
//                        event.setPost_time(jsonEvent.getString("post_dateTime"));
                        event.setLike_num(jsonEvent.getInt("like_num"));
                        event.setDislike_num(jsonEvent.getInt("dislike_num"));
                        event.setComment_num(jsonEvent.getInt("comment_num"));
                        event.setLocation(jsonEvent.getString("address"));
                        event.setPrice(jsonEvent.getString("price"));
                        event.setPost_date(jsonEvent.getString("post_dateTime").split(" ")[0]);
                        event.setPost_time(jsonEvent.getString("post_dateTime").split(" ")[1]);
//                        event.setContent(jsonEvent.getString("content"));

                        // number of images
                        if (!jsonEvent.getString("images").equals("null"))
                            event.setNumOfImages(jsonEvent.getString("images").split(",").length);
                        else event.setNumOfImages(0);

                        // parse cover image
                        String temp_cover = jsonEvent.getString("cover_pic");
                        if (temp_cover.startsWith("http")) {
                            // is a full url
                            event.setCover_image(temp_cover);
                        } else if (temp_cover.startsWith("/images")) {
                            event.setCover_image("http://42.2.152.193:8080" + temp_cover);
                        } else {
                            Log.v(TAG, "incorrect cover_pic image");
                        }
                        Log.v(TAG, "cover image found:" + event.getCover_image());

//
                        events.add(event);
                    }

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        return events;
    }

    public ArrayList<Event> parseEventsList_cardSingle(String JSONStr){
        ArrayList<Event> events = new ArrayList<>();

        if (JSONStr.contains("error") || JSONStr.length() == 0) {
            //will not be error
        } else {
            try {
                JSONArray events_json_array = new JSONArray(JSONStr);
                if (events_json_array.length() > 0) {
                    for (int i = 0; i < events_json_array.length(); i++) {
                        JSONObject jsonEvent = events_json_array.getJSONObject(i);
                        Event event = new Event();
                        event.setName(jsonEvent.getString("name"));
                        event.setEventId(jsonEvent.getInt("id"));
                        event.setAuthor(jsonEvent.getString("author_name"));
//                        event.setPost_time(jsonEvent.getString("post_dateTime"));
                        event.setPrice(jsonEvent.getString("price"));
                        event.setPost_date(jsonEvent.getString("post_dateTime").split(" ")[0]);
                        event.setPost_time(jsonEvent.getString("post_dateTime").split(" ")[1]);
//                        event.setContent(jsonEvent.getString("content"));

                        // number of images
                        if (!jsonEvent.getString("images").equals("null"))
                            event.setNumOfImages(jsonEvent.getString("images").split(",").length);
                        else event.setNumOfImages(0);

                        // parse cover image
                        String temp_cover = jsonEvent.getString("cover_pic");
                        if (temp_cover.startsWith("http")) {
                            // is a full url
                            event.setCover_image(temp_cover);
                        } else if (temp_cover.startsWith("/images")) {
                            event.setCover_image("http://42.2.152.193:8080" + temp_cover);
                        } else {
                            Log.v(TAG, "incorrect cover_pic image");
                        }
                        Log.v(TAG, "cover image found:" + event.getCover_image());

//
                        events.add(event);
                    }

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        return events;
    }


    public User parseUserObject(String JSONStr) {
        final User user = new User();

        try {
            JSONArray obj = new JSONArray(JSONStr);

            user.setEmail(obj.getJSONObject(0).getString("email"));
            user.setUsername(obj.getJSONObject(0).getString("name"));
            user.setLoginType(obj.getJSONObject(0).getInt("loginType"));
            user.setRegistrationDate(obj.getJSONObject(0).getString("reg_date"));
            user.setLast_login_date(obj.getJSONObject(0).getString("last_login_date"));
            user.setGender(obj.getJSONObject(0).getString("sex"));
            user.setDob(obj.getJSONObject(0).getString("dob"));
            user.setUserType(obj.getJSONObject(0).getInt("userType"));
            user.setPoint(obj.getJSONObject(0).getInt("points"));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return user;
    }

    public int parseUserEventNum(String json) throws JSONException {
        JSONArray jsonArray = new JSONArray(json);
        return Integer.parseInt(jsonArray.getJSONObject(0).getString("TotalEvents"));
    }

    public Event parseDetailedEvent(Event tempIncompleteEvent, String jsonResponse) throws JSONException {
        // parse json
        JSONArray jsonArray = new JSONArray(jsonResponse);
        JSONObject jsonObject = jsonArray.getJSONObject(0);

        // author info
        tempIncompleteEvent.setAuthor(jsonObject.getString("author_name"));
        tempIncompleteEvent.setAuthor_id(jsonObject.getString("user_id"));

        // event info
        tempIncompleteEvent.setContent(jsonObject.getString("content"));
        tempIncompleteEvent.setLocation(jsonObject.getString("address"));
        tempIncompleteEvent.setContactNo(jsonObject.getString("contact"));
//        tempIncompleteEvent.setLike_num(jsonObject.getInt("like_num"));
//        tempIncompleteEvent.setDislike_num(jsonObject.getInt("dislike_num"));
//        tempIncompleteEvent.setComment_num(jsonObject.getInt("comment_num"));

        if (!jsonObject.getString("start_dateTime").equals("null")) {
            Log.v(TAG, "start_dateTime : " + jsonObject.getString("start_dateTime"));
            tempIncompleteEvent.setFrom_date(jsonObject.getString("start_dateTime").split(" ")[0]);
            tempIncompleteEvent.setFrom_time(jsonObject.getString("start_dateTime").split(" ")[1]);
        }
        if (!jsonObject.getString("end_dateTime").equals("null")) {
            tempIncompleteEvent.setTo_date(jsonObject.getString("end_dateTime").split(" ")[0]);
            tempIncompleteEvent.setTo_time(jsonObject.getString("end_dateTime").split(" ")[1]);
        }
        tempIncompleteEvent.setLat(jsonObject.getString("lat"));
        tempIncompleteEvent.setLng(jsonObject.getString("lng"));

        // parse image from json
        if (!jsonObject.getString("images").equals("null")) {

            String allImages = jsonObject.getString("images");

            for (String singleImage : allImages.split(",")) {

                if (singleImage.startsWith("http")) {
                    // is a full url
                    tempIncompleteEvent.addImage(singleImage);

                } else if (singleImage.startsWith("/images")) {
                    // is located in our server
                    tempIncompleteEvent.addImage("http://42.2.152.193:8080" + singleImage);

                } else {
                    // wrong url
                    Log.v(TAG, "incorrect url image");
                    // use cover image
                    tempIncompleteEvent.addImage(tempIncompleteEvent.getCover_image());
                }

                Log.v(TAG,"image found:"+singleImage);

            }
        } else{
            Log.v(TAG, "empty images");
        }

        // profile pic
        if (!jsonObject.getString("profilePic").equals("null")) {
            String profilePic = jsonObject.getString("profilePic");

            if (profilePic.startsWith("http"))
                tempIncompleteEvent.setProfile_pic(jsonObject.getString("profilePic"));
            else if (profilePic.startsWith("/profilePic")) {
                tempIncompleteEvent.setProfile_pic("http://42.2.152.193:8080" + profilePic);
            }

        }else{
            // set default profile pic
            tempIncompleteEvent.setProfile_pic("http://42.2.152.193:8080/images/default_profile_pic.png");
        }

        return tempIncompleteEvent;
    }

    public String getKeyValueFromJson(String key, String JSONStr) throws JSONException {
        JSONObject obj = new JSONObject(JSONStr);

        return obj.getString(key);

    }

    public String[] parseJsonToAddress(String jsonResponse) throws JSONException {
        ArrayList<String> addresses = new ArrayList<>();
        JSONArray results = new JSONObject(jsonResponse).getJSONArray("results");

        for (int i = 0; i < results.length(); i++) {
            JSONObject address = results.getJSONObject(i);
            String addressString = address.getString("formatted_address");
//            System.out.println("Item " + i + " :" + addressString);
            addresses.add(addressString);
        }
        return addresses.toArray(new String[addresses.size()]);
    }


    /**
     * This method help parse the bookmark json into event arrayList
     * @param eventJsonResponse the json get from http://42.2.152.193:8080/events/json_get10BookmarkEvents_card.php?email=[user's email]&page=[page no.]
     * @return ArrayList<Event>
     */
    public ArrayList<Event> parseBookmarkEventsList_card(String eventJsonResponse) {
        ArrayList<Event> events = new ArrayList<>();

        if (eventJsonResponse.contains("error") || eventJsonResponse.length() == 0) {
            //will not be error
        } else {
            try {
                JSONArray events_json_array = new JSONArray(eventJsonResponse);
                if (events_json_array.length() > 0) {
                    for (int i = 0; i < events_json_array.length(); i++) {
                        JSONObject jsonEvent = events_json_array.getJSONObject(i);
                        Event event = new Event();
                        event.setName(jsonEvent.getString("name"));
                        event.setEventTypeName(jsonEvent.getString("type"));
                        event.setEventId(jsonEvent.getInt("event_id"));
                        event.setAuthor(jsonEvent.getString("author_name"));
                        event.setBookmark_time(jsonEvent.getString("dateTime"));
                        event.setLat(jsonEvent.getString("lat"));
                        event.setLng(jsonEvent.getString("lng"));
                        event.setLocation(jsonEvent.getString("address"));

                        // parse cover image
                        String temp_cover = jsonEvent.getString("cover_pic");
                        if (temp_cover.startsWith("http")) {
                            // is a full url
                            event.setCover_image(temp_cover);
                        } else if (temp_cover.startsWith("/images")) {
                            event.setCover_image("http://42.2.152.193:8080" + temp_cover);
                        } else {
                            Log.v(TAG, "incorrect cover_pic image");
                        }
                        Log.v(TAG, "cover image found:" + event.getCover_image());

//
                        events.add(event);
                    }

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        return events;
    }
}
