package com.winds.joinevents;

import android.content.Context;
import android.nfc.Tag;
import android.util.Log;

import com.squareup.okhttp.CacheControl;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by WindS on 8/10/2015.
 */
public class UserHandler {
    OkHttpClient okHttpClient;
    Context context;
    private final String TAG = "UserHandler";
    JSONHandler jsonHandler;
    SharedPreferenceManager sharedPreferenceManager;

    public UserHandler(Context context) {
        this.context = context;
        okHttpClient = new OkHttpClient();
        jsonHandler = new JSONHandler();
        sharedPreferenceManager = new SharedPreferenceManager(context);
    }

    /*
    * Pass user instance to create new user
    * @param. User object
    * @return success state
    * */
    public boolean createNewUser(User userToBeCreated) {
        try {
            //todo. need to encryt password?

            //todo. send registration day to server
            //show all info
            Log.v(TAG, "In CreateNewUser method");

            //return either original url or redirected url
            Map<String, String> params = new HashMap<String, String>();
            params.put("emailAddress", userToBeCreated.getEmail());
            params.put("accountName", userToBeCreated.getUsername());
            params.put("name", userToBeCreated.getName());
            params.put("sex", userToBeCreated.getGender());
            params.put("dob", userToBeCreated.getDob());
            params.put("password", userToBeCreated.getPassword());
            params.put("regDate", userToBeCreated.getRegistrationDate());
            params.put("userType", String.valueOf(userToBeCreated.getUserType()));
            params.put("points", "0");
            params.put("last_login_date", userToBeCreated.getLast_login_date());

            FormEncodingBuilder builder = new FormEncodingBuilder();
            for (String key : params.keySet()) {
                builder.add(key, params.get(key));
            }

            RequestBody formBody = builder.build();
            Request request = new Request.Builder()
                    .url("http://join-events.co.nf/post_newUser.php") // todo change address
                    .post(formBody)
                    .build();

            Response response = okHttpClient.newCall(request).execute();

            Log.v(TAG, "create user procedure ok!");

            System.out.println(response);

            return true;

        } catch (IOException e) {
            e.printStackTrace();
        }


        // return false if any problem happened
        return false;
    }


    /*
    * Check whether user is existed in DB
    * @param: email
    * @return: exist or not
    * */
    public boolean isUserExist(String email) throws IOException {

        Request request = new Request.Builder()
                .url("http://join-events.co.nf/users/isEmailExist.php?emailAddress=" + email)
                .build();

        Response response = okHttpClient.newCall(request).execute();

        // check querying db status
        if (!response.isSuccessful())
            throw new IOException("Unexpected code " + response);

        // if isempty means there is no existing user with same email address
        // otherwise will print out all user's details in JSON
        if (!response.body().string().isEmpty()) {

            Log.v(TAG, "response.body().String is not empty");


            return true;
        } else {
            Log.v(TAG, "response.body().String is empty");
            return false;
        }
    }


    /*
    * Check whether user is existed in DB
    * @param: email
    * @return: exist or not
    * */
    public boolean isUsernameExist(String username) throws IOException {

        Request request = new Request.Builder()
                .url("http://join-events.co.nf/users/isUsernameExist.php?username=" + username)
                .build();

        Response response = okHttpClient.newCall(request).execute();

        // check querying db status
        if (!response.isSuccessful())
            throw new IOException("Unexpected code " + response);

        // if isempty means there is no existing user with same email address
        // otherwise will print out all user's details in JSON
        if (!response.body().string().isEmpty() && response.body().string().equals("exist")) {

            Log.v(TAG, "username exist");
            return true;
        } else {
            Log.v(TAG, "username ok!");
            return false;
        }
    }

    public void changeLoginType(int type) {
        sharedPreferenceManager.changeLoginType(type);
    }

    public int getUserEventNumber(String userid) throws IOException, JSONException {
        int eventNum;

        Request request = new Request.Builder()
                .url("http://join-events.co.nf/users/number_getUserTotalEvents.php?user_id=" + userid)
                .build();

        Response response = okHttpClient.newCall(request).execute();

        String jsonResponse = response.body().string();
        // check querying db status
        if (!response.isSuccessful())
            throw new IOException("Unexpected code " + response);
        else
            eventNum = jsonHandler.parseUserEventNum(jsonResponse);

        return eventNum;
    }

    /*
    * Use the key: email to connect DB and grasp all user details of this email.
    * @param: email
    * @return: user object
    * */
    public User getAllUserDetails(String email) throws IOException {
        String userJson = "";

        //todo connect db and get all user's details
        Request request = new Request.Builder()
                .url("http://join-events.co.nf/users/isEmailExist.php?emailAddress=" + email)
                .build();

        Response response = okHttpClient.newCall(request).execute();

        // check querying db status
        if (!response.isSuccessful())
            throw new IOException("Unexpected code " + response);
        else
            userJson = response.body().string();

        /*
        *
        * [
	{
	"user_id":"365890",
	"emailAddress":"s.friday1004@gmail.com",
	"accountName":"wind",
	"name":"Sze Kit Fun",
	"sex":"m",
	"dob":"0000-00-00",
	"registrationDate":"0000-00-00",
	"password":"123456",
	"userType":"0",
	"loginType":"0",
	"last_login_date":"0000-00-00",
	"points":"0"
	}

]
        * */

        User user = jsonHandler.parseUserObject(userJson);
//        code


        return user;
    }


}
