package com.winds.joinevents;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.SwitchPreference;
import android.util.Log;
import android.widget.Toast;

public class SettingActivity extends android.preference.PreferenceActivity  {
//    private EditTextPreference mloginNamePre, mloginPasswordPre;
//    private ListPreference mUpdateTimePre, mTextSize_trading, mTextSize_detailed;
    private ListPreference mSync_frequency;
    private Preference mClearPre;
//    private SwitchPreference mUpdateTimeSwitch;

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    private static boolean isAnyPrefs = false;

    private final String PREFERENCE_NAME = "SettingPrefs";
    private final String TAG ="SettingActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        getSharedPreferences(PREFERENCE_NAME,Context.MODE_PRIVATE).edit().clear().commit();



        addPreferencesFromResource(R.xml.preferences);

        initUIComponents();

    }

    private void setOriginalComponentsValue(){
        editor.putBoolean("isServiceRunning",false);
        editor.commit();
//        mSync_frequency.setDefaultValue(30);
//        mloginNamePre.setText("");
//        mloginPasswordPre.setText("");
//        mUpdateTimePre.setDefaultValue("240");
//        mTextSize_trading.setDefaultValue(14);
//        mTextSize_detailed.setDefaultValue(18);
//        mUpdateTimeSwitch.setChecked(true);
//
//
//        mloginNamePre.setSummary(R.string.title_login_name);
//        mloginPasswordPre.setSummary(R.string.title_login_password);
//        mTextSize_trading.setSummary(R.string.summary_text_size_trading);
//        mTextSize_detailed.setSummary(R.string.summary_text_size_detailed);
//        mUpdateTimeSwitch.setSwitchTextOn(R.string.summary_allow_update);

    }
    private void initUIComponents(){
//        preference to edit
        preferences = getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        editor = preferences.edit();


////        login
//        mloginNamePre = (EditTextPreference)findPreference("login_name");
//        mloginPasswordPre = (EditTextPreference)findPreference("login_password");
//
//
////        notification
//        mUpdateTimeSwitch = (SwitchPreference)findPreference("pref_allow_update");
//        mUpdateTimePre = (ListPreference)findPreference("pref_update_time");
//
////        layout
//        mTextSize_trading = (ListPreference)findPreference("pref_text_size_trading");
//        mTextSize_detailed = (ListPreference)findPreference("pref_text_size_detailed");
//        mTextSize_detailed.setEnabled(false);
        //Sync settings
        mSync_frequency = (ListPreference) findPreference("pref_syn_frequency");

//        others
        mClearPre = findPreference("pref_clear_pref");


        setupOnChangeListener();

//        restoreAllValues();
    }

    private void setupOnChangeListener(){

//        sync settings
        mSync_frequency.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                editor.putInt("sync_frequency", Integer.valueOf(newValue.toString()));
                editor.apply();
                return true;
            }
        });

//        others


        mClearPre.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
//                onclick
                new AlertDialog.Builder(SettingActivity.this)
                        .setTitle("Confirmation dialog")
                        .setMessage("Are you sure to clear all settings ? ")
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Log.v(TAG, "negative button onclick");
                            }
                        })
                        .setPositiveButton("Clear", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                clearAllSharedPrefs();
                                Log.v(TAG, "positive button onclick");
                                Toast.makeText(getApplicationContext(),"清除完成",Toast.LENGTH_SHORT).show();
                            }
                        })
                        .show();

                return true;
            }
        });


    }


    private void restoreAllValues(){
////        login id
//        if (preferences.getString("Authentication_Id","")!="") {
//            mloginNamePre.setSummary(preferences.getString("Authentication_Id", ""));
//            Log.v(TAG,"set login name okay");
//        }
////        login password
//        if (preferences.getString("Authentication_Password", "")!=""){
//            mloginPasswordPre.setSummary(R.string.summary_login_password_after);
//            Log.v(TAG, "set password okay");
//        }
////        test size in trading post
//        if (preferences.getInt("TextSize_trading", 13)!=13){
//            mTextSize_trading.setDefaultValue(preferences.getInt("TextSize_trading", 13));
//        }
//
////        text size in detailed post
//        if (preferences.getInt("TextSize_detailed", 18) != 18) {
//            mTextSize_detailed.setDefaultValue(preferences.getInt("TextSize_detailed", 18));
//        }
//
////        updateSwitch
//        if(preferences.getBoolean("update_switch",true)){
//            Log.v(TAG,"restore switch checked : true");
//            mUpdateTimeSwitch.setChecked(true);
//        }else {
//            Log.v(TAG,"restore switch checked : false");
//            mUpdateTimeSwitch.setChecked(false);
//        }
//
////        update time
//        if (preferences.getInt("Update_Time", 240) != 240) {
//            mUpdateTimePre.setDefaultValue(preferences.getInt("Update_Time", 240));
//        }

    }


    private void clearAllSharedPrefs(){
//        clear all preference
        editor.clear().commit();

        //todo call originalComponentsValue to set all the components' original
        setOriginalComponentsValue();
//        isAnyPrefs = false;
    }


}

